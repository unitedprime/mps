package com.solt.mps.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("longTextConverter")
public class SpaceAdder implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if(null != value) {
			String string = String.valueOf(value);
			
			string = string.replaceAll("ပါတီ", "ပါတီ ");
			string = string.replaceAll("ဒီမိုကရေစီ", "ဒီမိုကရေစီ ");
			string = string.replaceAll("အမှုဆောင်", "အမှုဆောင် ");
			string = string.replaceAll("ကော်မတီ", "ကော်မတီ ");
			string = string.replaceAll("ကိုယ်စားလှယ်", " ကိုယ်စားလှယ်");
			string = string.replaceAll("ဝန်ကြီးဌာန", " ဝန်ကြီးဌာန");
			string = string.replaceAll("မန်နေဂျာ", "မန်နေဂျာ ");
			string = string.replaceAll("အဖွဲ့", " အဖွဲ့");
			string = string.replace("(", " (");
			string = string.replace("/", "/ ");
			string = string.replace("၊", "၊ ");
			string = string.replace(")", ") ");
			return string;
		}
		return null;
	}

}
