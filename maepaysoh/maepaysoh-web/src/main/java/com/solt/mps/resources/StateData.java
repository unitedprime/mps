package com.solt.mps.resources;

import java.io.Serializable;

public class StateData implements Serializable, Comparable<StateData> {

	private static final long serialVersionUID = 1L;

	private String stateName;
	private String statePCode;
	private String amPCode;

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStatePCode() {
		return statePCode;
	}

	public void setStatePCode(String statePCode) {
		this.statePCode = statePCode;
	}

	public String getAmPCode() {
		return amPCode;
	}

	public void setAmPCode(String amPCode) {
		this.amPCode = amPCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amPCode == null) ? 0 : amPCode.hashCode());
		result = prime * result
				+ ((stateName == null) ? 0 : stateName.hashCode());
		result = prime * result
				+ ((statePCode == null) ? 0 : statePCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateData other = (StateData) obj;
		if (amPCode == null) {
			if (other.amPCode != null)
				return false;
		} else if (!amPCode.equals(other.amPCode))
			return false;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		if (statePCode == null) {
			if (other.statePCode != null)
				return false;
		} else if (!statePCode.equals(other.statePCode))
			return false;
		return true;
	}

	@Override
	public int compareTo(StateData o) {
		return this.getStatePCode().compareTo(o.getStatePCode());
	}

}
