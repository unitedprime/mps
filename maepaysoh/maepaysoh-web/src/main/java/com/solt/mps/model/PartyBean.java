package com.solt.mps.model;

import java.io.Serializable;
import java.util.Map;
import java.util.function.Predicate;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.unitedprime.mps.cach.PartyCachService;
import org.unitedprime.mps.model.Party;

import com.solt.mps.model.lazymodel.PartyDataLoadListener;
import com.solt.mps.model.lazymodel.PartyLazyList;
import com.solt.mps.util.MyanmarFontConverter;

@Named
@ViewScoped
public class PartyBean implements Serializable, PartyDataLoadListener {
    private static final long serialVersionUID = 1L;
    @Inject
    private PartyLazyList partyList;
    @Inject
    private PartyCachService partyService;
    @Inject
    private BaseBean baseBean;

    private String partyName;
    private String shortName;
    private String chairman;

    @Inject
    private Event<Party> viewPartyEvent;

    private Predicate<String> notNull = a -> null != a && !a.isEmpty();

    private Party party;
    private int partyId;

    @PostConstruct
    private void init() {
	this.partyList.setListener(this);
    }

    public void getPartyInfoById() {
	party = partyService.findById(partyId);
	viewPartyEvent.fire(party);
    }

    public String navigateToPartyDetail() {

	FacesContext context = FacesContext.getCurrentInstance();
	Map<String, String> params = context.getExternalContext()
		.getRequestParameterMap();
	partyId = Integer.parseInt(params.get("partyId"));

	return "/pages/party/partyDetail.xhtml?faces-redirect=true&includeViewParams=true";
    }

    public LazyDataModel<Party> getPartyList() {
	return partyList;
    }

    public void setPartyList(PartyLazyList partyList) {
	this.partyList = partyList;
    }

    public Party getParty() {
	return party;
    }

    public void setParty(Party party) {
	this.party = party;
    }

    public int getPartyId() {
	return partyId;
    }

    public void setPartyId(int partyId) {
	this.partyId = partyId;
    }

    public String getPartyName() {
	return partyName;
    }

    public void setPartyName(String partyName) {
	this.partyName = partyName;
    }

    public String getShortName() {
	return shortName;
    }

    public void setShortName(String shortName) {
	this.shortName = shortName;
    }

    public String getChairman() {
	return chairman;
    }

    public void setChairman(String chairman) {
	this.chairman = chairman;
    }

    @Override
    public Predicate<Party> getParams() {
	Predicate<Party> pred = a -> true;

	if (notNull.test(partyName)) {
	    if (baseBean.getLanguageCookie().equals("zawgyi")) {
		partyName = MyanmarFontConverter.getZ2Uni(partyName.trim());
	    }
	    pred = pred.and(a -> a.getName().contains(partyName));
	}

	if (notNull.test(shortName)) {
	    if (baseBean.getLanguageCookie().equals("zawgyi")) {
		shortName = MyanmarFontConverter.getZ2Uni(shortName.trim());
	    }
	    pred = pred.and(a -> a.getAbbreviation().contains(shortName));
	}

	if (notNull.test(chairman)) {
	    if (baseBean.getLanguageCookie().equals("zawgyi")) {
		chairman = MyanmarFontConverter.getZ2Uni(chairman.trim());
	    }
	    pred = pred.and(a -> a.getChairmans().contains(chairman)
		    || a.getLeaders().contains(chairman));
	}

	return pred;
    }

    public String getPartyNamePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("ပါတီအမည်");
	}
	return "ပါတီအမည်";
    }

    public String getPartyLeaderPlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("ပါတီဥက္ကဌ");
	}
	return "ပါတီဥက္ကဌ";
    }

    public String getPartyShortNamePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("ပါတီအမည် အတိုကောက်");
	}
	return "ပါတီအမည် အတိုကောက်";
    }
}
