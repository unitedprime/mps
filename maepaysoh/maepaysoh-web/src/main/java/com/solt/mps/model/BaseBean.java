package com.solt.mps.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Named
@ViewScoped
public class BaseBean implements Serializable {
    private static final long serialVersionUID = 1L;

    public void saveLanguageCookie(String value) {
	FacesContext facesContext = FacesContext.getCurrentInstance();
	Cookie languageCookie = new Cookie("lang", value);
	languageCookie.setMaxAge(99999999);
	((HttpServletResponse) facesContext.getExternalContext().getResponse())
		.addCookie(languageCookie);
	ExternalContext ec = FacesContext.getCurrentInstance()
		.getExternalContext();
	try {
	    ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
	} catch (IOException e) {
	}
    }

    public String getLanguageCookie() {
	Map<String, Object> cookies = FacesContext.getCurrentInstance()
		.getExternalContext().getRequestCookieMap();
	Cookie cookie = null;
	if (cookies.get("lang") != null) {
	    cookie = (Cookie) cookies.get("lang");
	}
	if (cookie == null) {
	    saveLanguageCookie("uni");
	    return "uni";
	} else {
	    return cookie.getValue();
	}

    }

}
