package com.solt.mps.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.api.GeoResources.Param;
import org.unitedprime.mps.model.GeoListResult;
import org.unitedprime.mps.resources.GeoResource;

@ApplicationScoped
public class StateResources implements Serializable {

	private static final long serialVersionUID = 1L;

	@Named
	@Produces
	private List<StateData> stateData;

	@Inject
	@GeoResource(GeoType.State)
	private GeoResources geoState;

	@PostConstruct
	public void init() {

		stateData = new ArrayList<>();

		Map<String, StateData> resultMap = new TreeMap<>();
		Map<Param, Object> params = new HashMap<>();
		params.put(Param.per_page, 200);
		params.put(Param.no_geo, true);

		GeoListResult result = geoState.find(params);

		result.getData().stream().map(a -> {
			StateData data = new StateData();
			data.setStateName(a.getProperties().get("constituency_name_my"));
			data.setStatePCode(a.getProperties().get("ST_PCODE"));
			data.setAmPCode(a.getProperties().get("AM_PCODE"));
			return data;
		}).distinct().forEach(a -> {
			resultMap.put(a.getStatePCode().trim(), a);
		});

		StateData std = new StateData();
		std.setStateName("ပြည်နယ်/တိုင်း");
		std.setStatePCode("");
		std.setAmPCode("");
		stateData.add(std);

		stateData.addAll(resultMap.values());
	}

	public StateData findByStatePcode(String statePcode) {
		return stateData.stream()
				.filter(a -> a.getStatePCode().equals(statePcode)).findAny()
				.orElse(null);
	}

	public List<StateData> getStateData() {
		return stateData;
	}

	public void setStateData(List<StateData> stateData) {
		this.stateData = stateData;
	}

}
