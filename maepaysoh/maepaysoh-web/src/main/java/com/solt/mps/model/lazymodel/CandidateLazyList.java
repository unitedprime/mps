package com.solt.mps.model.lazymodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.unitedprime.mps.api.CandidateResources.Param;
import org.unitedprime.mps.business.CandidateService;
import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.Candidates;

public class CandidateLazyList extends LazyDataModel<Candidate> {

	private static final long serialVersionUID = 1L;

	private CandidateDataLoadListener loadListener;

	public void setLoadListener(CandidateDataLoadListener loadListener) {
		this.loadListener = loadListener;
	}

	public CandidateLazyList() {
	}

	@Inject
	private CandidateService service;
	private Candidates candidates;

	@Override
	public List<Candidate> load(int startingAt, int maxPerPage,
			String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		Map<Param, Object> paramMap = new HashMap<>();
		paramMap.put(Param.page, (startingAt / maxPerPage) + 1);
		paramMap.put(Param.per_page, maxPerPage);

		loadListener.setParams(paramMap);
		
		candidates = service.getAllCandidates(paramMap);
		setRowCount(candidates.getMeta().getPagination().getTotal());
		setPageSize(maxPerPage);

		return candidates.getData();
	}

}
