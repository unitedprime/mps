package com.solt.mps.util;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.unitedprime.mps.api.CandidateResources;
import org.unitedprime.mps.api.CandidateResources.Param;
import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.Candidates;
import org.unitedprime.mps.model.Geo;
import org.unitedprime.mps.model.GeoListResult;
import org.unitedprime.mps.resources.GeoResource;

import com.solt.mps.resources.StateData;
import com.solt.mps.resources.StateResources;

@Named
@ViewScoped
public class GeoLocation implements Serializable {
	private static final long serialVersionUID = 1L;
	private double longitude;
	private double latitude;
	private List<Candidate> candidateList;
	private String legislature = "ပြည်သူ့လွှတ်တော်";

	@Inject
	@GeoResource(GeoType.Township)
	private GeoResources tspResource;

	@Inject
	private StateResources stateRes;
	@Inject
	private CandidateResources candidateRes;
	
	public void loadLocalizeParties() throws IOException {
		GeoListResult result = tspResource.find(longitude, latitude);
		if (result != null && result.getData() != null
				&& result.getData().size() > 0) {
			getCandidates(result.getData().get(0));
		}
	}

	private void getCandidates(Geo geo) throws IOException {
		candidateList = null;
		if (geo != null) {

			Map<Param, Object> paramMap = new HashMap<CandidateResources.Param, Object>();
			paramMap.put(Param.legislature, legislature);
			if (legislature.equals("ပြည်သူ့လွှတ်တော်")) {
				paramMap.put(Param.constituency_ts_pcode, geo.getProperties()
						.get("TS_PCODE"));
			} else if (legislature.equals("အမျိုးသားလွှတ်တော်")) {
				String state_pcode = geo.getProperties().get("ST_PCODE");
				StateData state = stateRes.findByStatePcode(state_pcode);
				paramMap.put(Param.constituency_st_pcode, state_pcode);
				paramMap.put(Param.constituency_am_pcode, state.getAmPCode());

			} else if (legislature.equals("တိုင်းဒေသကြီး/ပြည်နယ် လွှတ်တော်")) {
				paramMap.put(Param.legislature, legislature);
				paramMap.put(Param.constituency_ts_pcode, geo.getProperties()
						.get("TS_PCODE"));
			}
			
			Candidates candidates = candidateRes.find(paramMap);
			candidateList = candidates.getData();
			for (Candidate candidate : candidateList) {
				if (candidate.getPhoto().equals("0")) {
					candidate
							.setPhoto("https://www.myapnea.org/assets/default-user-3684583869e00883d4808b2734254632bf10f458cc3b1b7ac8cc16848507ba2f.jpg");
					System.err.println(candidate.getPhoto());
					URL u = new URL(candidate.getPhoto());
					HttpURLConnection huc = (HttpURLConnection) u
							.openConnection();
					huc.setRequestMethod("GET");
					huc.connect();
					int code = huc.getResponseCode();
					if (code == 404) {
						candidate
								.setPhoto("https://www.myapnea.org/assets/default-user-3684583869e00883d4808b2734254632bf10f458cc3b1b7ac8cc16848507ba2f.jpg");
					}
				}
			}
		}
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public List<Candidate> getCandidateList() {
		return candidateList;
	}

	public void setCandidateList(List<Candidate> candidateList) {
		this.candidateList = candidateList;
	}

	public String getLegislature() {
		return legislature;
	}

	public void setLegislature(String legislature) {
		this.legislature = legislature;
	}

}
