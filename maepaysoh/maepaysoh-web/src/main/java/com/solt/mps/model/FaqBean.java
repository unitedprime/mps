package com.solt.mps.model;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.unitedprime.mps.api.FaqResources.Param;

import com.solt.mps.model.lazymodel.FaqDataLoadListener;
import com.solt.mps.model.lazymodel.FaqLazyList;
import com.solt.mps.util.MyanmarFontConverter;

@Named
@ViewScoped
public class FaqBean implements Serializable, FaqDataLoadListener {
    private static final long serialVersionUID = 1L;
    @Inject
    private FaqLazyList faqList;

    private String keyword;
    private String lawOrSource;
    @Inject
    private BaseBean baseBean;

    @PostConstruct
    public void init() {
	faqList.setListener(this);
    }

    public FaqLazyList getFaqList() {
	return faqList;
    }

    public void setFaqList(FaqLazyList faqList) {
	this.faqList = faqList;
    }

    public String getKeyword() {
	return keyword;
    }

    public void setKeyword(String keyword) {
	this.keyword = keyword;
    }

    public String getLawOrSource() {
	return lawOrSource;
    }

    public void setLawOrSource(String lawOrSource) {
	this.lawOrSource = lawOrSource;
    }

    @Override
    public void setParam(Map<Param, Object> paramMap) {
	if (null != keyword && !keyword.trim().isEmpty()) {
	    if (baseBean.getLanguageCookie().equals("zawgyi")) {
		keyword = MyanmarFontConverter.getZ2Uni(keyword.trim());
	    }
	    paramMap.put(Param.q, keyword.trim());
	}

	if (null != lawOrSource && !lawOrSource.isEmpty()) {
	    paramMap.put(Param.section, lawOrSource);
	}
    }

    public String getAllQuestionTypePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("မေးခွန်းအမျိုးအစားအားလုံး");
	}
	return "မေးခွန်းအမျိုးအစားအားလုံး";
    }

    public String getKeyWordPlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("ရှာဖွေရန်စကားလုံး");
	}
	return "ရှာဖွေရန်စကားလုံး";
    }

}
