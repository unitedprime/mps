package com.solt.mps.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("mmDateFormatter")
public class MyanmarDateFormat implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		try {
			long lgDate = (Long)value;
			
			if(lgDate != 0) {
				Date date = new Date(lgDate);
				
				String strDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
				
				return MyanmarNumberConverter.getMyanmar(strDate);
			} else {
				return "ဖော်ပြထားခြင်းမရှိပါ";
			}
			
		} catch (Exception e) {
			
		}
		
		return null;
	}

}
