package com.solt.mps.model.lazymodel;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.unitedprime.mps.cach.PartyCachService;
import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.Party;

public class PartyLazyList extends LazyDataModel<Party> {

	private static final long serialVersionUID = 1L;

	private Parties parties;
	private BiFunction<Integer, Integer, Integer> startPageNumber = (a, b) -> (a.intValue() / b.intValue()) + 1;
	
	private PartyDataLoadListener listener;
	
	@Inject
	private PartyCachService partyService;
	

	@Override
	public List<Party> load(int startingAt, int maxPerPage, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		
		parties = partyService.find(listener.getParams(), maxPerPage,
				startPageNumber.apply(startingAt,maxPerPage));
		
		setRowCount(parties.getMeta().getCount());
		setPageSize(maxPerPage);

		return parties.getData();
	}

	public void setListener(PartyDataLoadListener listener) {
		this.listener = listener;
	}

}
