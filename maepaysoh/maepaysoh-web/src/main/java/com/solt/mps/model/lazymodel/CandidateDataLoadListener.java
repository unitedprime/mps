package com.solt.mps.model.lazymodel;

import java.util.Map;

import org.unitedprime.mps.api.CandidateResources.Param;

public interface CandidateDataLoadListener {
	
	void setParams(Map<Param, Object> params);

}
