package com.solt.mps.util;

import java.util.Arrays;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("mmNumberConverter")
public class MyanmarNumberConverter implements Converter {
	
	private static final List<String> NUMBERS = Arrays.asList("၀","၁","၂","၃","၄","၅","၆","၇","၈","၉");
	
	public static String getMyanmar(String origin) {
		StringBuffer sb = new StringBuffer();
		for(char c : origin.toCharArray()) {
			String s = String.valueOf(c);
	  		try {
	  			int index = Integer.parseInt(s);
	  			sb.append(NUMBERS.get(index));
			} catch (Exception e) {
				sb.append(s);
			}
		}
		
		return sb.toString();
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if(null != value) {
			String str = String.valueOf(value);
			return getMyanmar(str);
		}
		return null;
	}

}
