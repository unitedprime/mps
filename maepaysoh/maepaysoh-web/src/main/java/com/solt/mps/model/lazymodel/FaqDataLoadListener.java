package com.solt.mps.model.lazymodel;

import java.util.Map;

import org.unitedprime.mps.api.FaqResources.Param;

public interface FaqDataLoadListener {
	void setParam(Map<Param, Object> paramMap);
}
