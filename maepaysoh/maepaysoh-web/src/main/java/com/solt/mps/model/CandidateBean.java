package com.solt.mps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.unitedprime.mps.api.CandidateResources.Param;
import org.unitedprime.mps.business.CandidateService;
import org.unitedprime.mps.cach.PartyCachService;
import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.Party;

import com.solt.mps.model.lazymodel.CandidateDataLoadListener;
import com.solt.mps.model.lazymodel.CandidateLazyList;
import com.solt.mps.util.MyanmarFontConverter;

@Named
@ViewScoped
public class CandidateBean implements Serializable, CandidateDataLoadListener {

    private static final long serialVersionUID = 1L;
    @Inject
    private CandidateLazyList candidateList;
    @Inject
    private CandidateService service;
    private Candidate candidate;
    private String schLegislature;
    private String schStatePcode;
    private String schCandidateName;
    private String schPartyName;

    private String candidateId;
    private int partyId;
    @Inject
    private Event<Candidate> viewCandidateEvent;
    @Inject
    private List<String> partyNameList;
    @Inject
    private PartyCachService partyService;
    @Inject
    private BaseBean baseBean;

    private List<String> legislatures = Arrays.asList("လွှတ်တော်အမျိုးအစား",
	    "ပြည်သူ့လွှတ်တော်", "အမျိုးသားလွှတ်တော်",
	    "တိုင်းဒေသကြီး/ပြည်နယ် လွှတ်တော်");

    @PostConstruct
    public void init() {
	candidateList.setLoadListener(this);
    }

    public String navigateToCandidateDetail() {

	FacesContext context = FacesContext.getCurrentInstance();
	Map<String, String> params = context.getExternalContext()
		.getRequestParameterMap();
	candidateId = params.get("candidateId");

	return "/pages/candidate/candidateDetail.xhtml?faces-redirect=true&includeViewParams=true";
    }

    public String searchCandidateListByPartyLegislature() {
	FacesContext context = FacesContext.getCurrentInstance();
	Map<String, String> params = context.getExternalContext()
		.getRequestParameterMap();
	schLegislature = params.get("legislature");
	if (params.get("partyId") != null
		&& !params.get("partyId").equals("null")) {
	    partyId = Integer.parseInt(params.get("partyId"));
	}
	return "/pages/candidate/candidates.xhtml?faces-redirect=true&includeViewParams=true";
    }

    public List<String> completeText(String query) {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    query = MyanmarFontConverter.getZ2Uni(query);
	}
	List<String> results = new ArrayList<String>();
	for (String pname : partyNameList) {
	    if (pname.startsWith(query)) {
		results.add(pname);
	    }
	}
	return results;
    }

    public void getCandidateInfoById(String id) {
	candidate = service.findById(id);
	viewCandidateEvent.fire(candidate);
    }

    public LazyDataModel<Candidate> getCandidateList() {
	return candidateList;
    }

    public void setCandidateList(CandidateLazyList candidateList) {
	this.candidateList = candidateList;
    }

    public Candidate getCandidate() {
	return candidate;
    }

    public void setCandidate(Candidate candidate) {
	this.candidate = candidate;
    }

    public List<String> getLegislatures() {
	return legislatures;
    }

    public void setLegislatures(List<String> legislatures) {
	this.legislatures = legislatures;
    }

    @Override
    public void setParams(Map<Param, Object> paramMap) {

	if (null != schCandidateName && !schCandidateName.trim().isEmpty()) {
	    if (baseBean.getLanguageCookie().equals("zawgyi")) {
		schCandidateName = MyanmarFontConverter
			.getZ2Uni(schCandidateName.trim());
	    }
	    paramMap.put(Param.q, schCandidateName.trim());
	}

	if (null != schStatePcode && !schStatePcode.trim().isEmpty()
		&& !"0".equals(schStatePcode.trim())) {
	    paramMap.put(Param.constituency_st_pcode, schStatePcode.trim());
	}

	if (null != schLegislature && !schLegislature.trim().isEmpty()) {
	    if (!this.legislatures.get(0).equals(schLegislature))
		paramMap.put(Param.legislature, schLegislature.trim());
	}
	if (partyId > 0) {
	    paramMap.put(Param.party, partyId);
	}

	if (null != schPartyName && !schPartyName.trim().isEmpty()) {
	    if (baseBean.getLanguageCookie().equals("zawgyi")) {
		schPartyName = MyanmarFontConverter.getZ2Uni(schPartyName
			.trim());
	    }
	    Party p = partyService
		    .find(a -> a.getName().equals(schPartyName), 1, 1)
		    .getData().stream().findAny().orElse(null);

	    if (null != p) {
		paramMap.put(Param.party, p.getId());
	    }
	}

    }

    public String getSchLegislature() {
	return schLegislature;
    }

    public void setSchLegislature(String schLegislature) {
	this.schLegislature = schLegislature;
    }

    public String getSchStatePcode() {
	return schStatePcode;
    }

    public void setSchStatePcode(String schStatePcode) {
	this.schStatePcode = schStatePcode;
    }

    public String getSchCandidateName() {
	return schCandidateName;
    }

    public void setSchCandidateName(String schCandidateName) {
	this.schCandidateName = schCandidateName;
    }

    public String getCandidateId() {
	return candidateId;
    }

    public void setCandidateId(String candidateId) {
	this.candidateId = candidateId;
    }

    public int getPartyId() {
	return partyId;
    }

    public void setPartyId(int partyId) {
	this.partyId = partyId;
    }

    public String getSchPartyName() {
	return schPartyName;
    }

    public void setSchPartyName(String schPartyName) {
	this.schPartyName = schPartyName;
    }

    public List<String> getPartyNameList() {
	return partyNameList;
    }

    public void setPartyNameList(List<String> partyNameList) {
	this.partyNameList = partyNameList;
    }

    public String getNamePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("အမည်");
	}
	return "အမည်";
    }

    public String getPartyNamePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("ပါတီအမည်");
	}
	return "ပါတီအမည်";
    }

}
