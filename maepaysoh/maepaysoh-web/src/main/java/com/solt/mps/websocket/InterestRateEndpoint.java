package com.solt.mps.websocket;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.unitedprime.mps.interest.CandidateInterest;
import org.unitedprime.mps.interest.Interest;
import org.unitedprime.mps.interest.WsEvent;
import org.unitedprime.mps.interest.SessionRegistroy;
import org.unitedprime.mps.interest.WsEvent.Type;
import org.unitedprime.mps.utils.Loggable;

import com.solt.mps.util.MyanmarNumberConverter;

@Loggable
@ServerEndpoint("/interestRate")
public class InterestRateEndpoint {

	@Inject
	private SessionRegistroy sessions;
	@Inject
	private Event<WsEvent> wsEvent;

	@OnOpen
	public void onOpen(Session session, EndpointConfig conf) {
		sessions.add(session);
	}

	@OnClose
	public void onClose(Session session, CloseReason close) {
		sessions.remove(session);
	}

	@OnMessage
	public void onMessage(String message, Session session) {

		String [] strs = message.split(":");
		if(strs.length > 1) {
			WsEvent event = new WsEvent();

			switch (strs[0]) {
			case "refreshParty":
			case "RefreshParty":
				event.setPartyId(Integer.parseInt(strs[1]));
				event.setType(Type.RefreshParty);
				break;

			case "Policy":
			case "policy":
				event.setPartyId(Integer.parseInt(strs[1]));
				event.setType(Type.ViewPolicy);
				break;

			case "refreshCandidate":
			case "RefreshCandidate":
				event.setCandidateId(strs[1]);
				event.setType(Type.RefreshCandidate);
				break;

			default:
				break;
			}
			
			wsEvent.fire(event);
		} 

	}

	public void partyInterestSend(@Observes Interest partyInterest) {
		String result = jsonString(partyInterest);
		sessions.getAll().forEach(a -> a.getAsyncRemote().sendText(result));
	}

	public void candidateInterestSend(@Observes CandidateInterest interest) {
		sessions.getAll().forEach(
				a -> a.getAsyncRemote().sendText(jsonString(interest)));
	}

	public String jsonString(Interest interest) {
		
		final JsonObject jsonObject = Json.createObjectBuilder()
				.add("partyId", interest.getPartyId())
				.add("party", myanmar(interest.getPartyInterest()))
				.add("candidate", myanmar(interest.getCandidateInterest()))
				.add("policy", myanmar(interest.getPolicyInterest())).build();
		return jsonObject.toString();
	}

	public String jsonString(CandidateInterest candidate) {

		JsonObjectBuilder builder = Json.createObjectBuilder()
				.add("cId", candidate.getId())
				.add("cInterest", myanmar(candidate.getInterest()));
		
		if(candidate.getPartyInterest() != null) {
			builder = builder.add("partyId", candidate.getPartyInterest().getPartyId())
					.add("party", myanmar(candidate.getPartyInterest().getPartyInterest()))
					.add("candidate",myanmar(candidate.getPartyInterest().getCandidateInterest()))
					.add("policy", myanmar(candidate.getPartyInterest().getPolicyInterest()));
		}

		return builder.build().toString();
	}
	
	private String myanmar(Integer num) {
		String str = num.toString();
		return MyanmarNumberConverter.getMyanmar(str);
	}
}
