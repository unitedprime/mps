package com.solt.mps.model;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.unitedprime.mps.business.VoterService;
import org.unitedprime.vote.Voter;

import com.solt.mps.util.MyanmarFontConverter;

@Named
@ViewScoped
public class CheckVoter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Inject
    VoterService voterService;

    @Inject
    private BaseBean baseBean;

    private boolean nrcFlg = true;
    private Voter voter = new Voter();

    public Voter getVoter() {
	return voter;
    }

    public void setVoter(Voter voter) {
	this.voter = voter;
    }

    public boolean isNrcFlg() {
	return nrcFlg;
    }

    public void setNrcFlg(boolean nrcFlg) {
	this.nrcFlg = nrcFlg;
    }

    public void searchVoter() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    if (nrcFlg) {
		voter.setNrc(MyanmarFontConverter.getZ2Uni(voter.getNrc()));
	    }
	    voter.setVoter(MyanmarFontConverter.getZ2Uni(voter.getVoter()));
	}
	if (nrcFlg) {
	    voter = voterService.check(voter.getVoter().trim(), voter.getNrc()
		    .trim());
	} else {
	    voter = voterService.check(voter.getVoter().trim(), voter.getDob());
	}
	if (null == voter) {
	    voter = new Voter();
	    FacesContext.getCurrentInstance().addMessage(
		    null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!",
			    "မဲစာရင်းတွင် သင်၏ အချက်အလက်များရှာမတွေ့ပါ။"));
	}

    }

    public String getNamePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("အမည်");
	}
	return "အမည်";
    }

    public String getSelectItemNrc() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("မှတ်ပုံတင်");
	}
	return "မှတ်ပုံတင်";
    }

    public String getSelectItemDob() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("မွေးနေ့");
	}
	return "မွေးနေ့";
    }

    public String getNrcPlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("နိုင်ငံသားမှတ်ပုံတင်အမှတ်");
	}
	return "နိုင်ငံသားမှတ်ပုံတင်အမှတ်";
    }

    public String getDatePlaceHolder() {
	if (baseBean.getLanguageCookie().equals("zawgyi")) {
	    return MyanmarFontConverter.getUni2Z("ရက်-လ-နှစ်");
	}
	return "ရက်-လ-နှစ်";
    }

}
