package com.solt.mps.model.lazymodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.unitedprime.mps.api.FaqResources.Param;
import org.unitedprime.mps.cach.FaqCachService;
import org.unitedprime.mps.model.Faq;
import org.unitedprime.mps.model.Faqs;

public class FaqLazyList extends LazyDataModel<Faq> {

	private static final long serialVersionUID = 1L;

	private Faqs faqs;

	@Inject
	private FaqCachService service;
	
	private FaqDataLoadListener listener;
	
	public void setListener(FaqDataLoadListener listener) {
		this.listener = listener;
	}

	@Override
	public List<Faq> load(int startingAt, int maxPerPage, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		
		Map<Param, Object> paramMap = new HashMap<>();
		listener.setParam(paramMap);
		
		Supplier<Predicate<Faq>> supplier = () -> {
			Object keyword = paramMap.get(Param.q);
			
			Predicate<Faq> pred = a -> true;
			
			if(null != keyword) {
				String q = (String)keyword;
				if(!q.isEmpty()) {
					
					pred = pred.and(a -> a.getAnswer().contains(q) || 
							a.getQuestion().contains(q) ||
							a.getArticleOrSection().contains(q) ||
							a.getCategory().contains(q) ||
							a.getLawOrSource().contains(q) ||
							a.getType().contains(q));
				}
			}
			
			Object lawOrSource = paramMap.get(Param.section);
			
			if(null != lawOrSource) {
				String q = (String)lawOrSource;
				
				if(!q.isEmpty()) {
					pred = pred.and(a -> a.getLawOrSource().equals(q));
				}
			}
			
			return pred;
		};
		
		faqs = service.find(supplier.get(), (startingAt / maxPerPage) + 1,  maxPerPage);

		setRowCount(faqs.getMeta().getPagination().getTotal());
		setPageSize(maxPerPage);

		return faqs.getData();
	}


}
