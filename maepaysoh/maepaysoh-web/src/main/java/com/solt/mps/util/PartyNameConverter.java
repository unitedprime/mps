package com.solt.mps.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import org.unitedprime.mps.cach.PartyCachService;
import org.unitedprime.mps.model.Party;

@FacesConverter("partyNameConverter")
public class PartyNameConverter implements Converter {
	
	@Inject
	private PartyCachService partyResource;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		try {
			if(null != value) {
				int partyId = Integer.parseInt(value.toString());
				Party party = partyResource.findById(partyId);
				return party.getName();
			}
		} catch (Exception e) {
			
		}
		return null;
	}

}
