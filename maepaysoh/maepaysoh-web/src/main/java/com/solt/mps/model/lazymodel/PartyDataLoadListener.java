package com.solt.mps.model.lazymodel;

import java.util.function.Predicate;

import org.unitedprime.mps.model.Party;

public interface PartyDataLoadListener {
	Predicate<Party> getParams();
}
