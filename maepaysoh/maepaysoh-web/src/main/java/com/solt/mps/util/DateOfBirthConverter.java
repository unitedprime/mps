package com.solt.mps.util;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("AgeConverter")
public class DateOfBirthConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		try {
			if(null != value) {
				Long unixDate = Long.valueOf(value.toString()) * 1000;
				Date date = new Date(unixDate);
				Period period = Period.between(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), LocalDate.now());
				
				if(period.getYears() > 0) {
					return MyanmarNumberConverter.getMyanmar(String.valueOf(period.getYears())) + " နှစ်" ;
				}
				
				return "ဖော်ပြထားခြင်းမရှိပါ";
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
