$(document).ready(function() {
	var pathname = window.location.pathname;
   if (pathname.indexOf("home") >= 0) {
		$(".ui-paginator").css('display', 'none');
		$("#frmGeo\\:latitude").change(function(e){
			loadLocalizeParties();
		});
	}
		 if (navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function (p) {
			    	 if (pathname.indexOf("home") >= 0) {
						$("#frmGeo\\:longitude").val(p.coords.longitude);
					    $("#frmGeo\\:latitude").val(p.coords.latitude).trigger("change");
			    	 }
			        var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
			        var mapOptions = {
			            center: LatLng,
			            zoom: 16,
			            mapTypeId: google.maps.MapTypeId.ROADMAP
			        };
			        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
			        var marker = new google.maps.Marker({
			            position: LatLng,
			            map: map,
			            title: "<div style = 'height:60px;width:200px'><b>Your location:</b><br />Latitude: " + p.coords.latitude + "<br />Longitude: " + p.coords.longitude
			        });
			        google.maps.event.addListener(marker, "click", function (e) {
			            var infoWindow = new google.maps.InfoWindow();
			            infoWindow.setContent(marker.title);
			            infoWindow.open(map, marker);
			        });
			    });
			} else {
			    alert('Geo Location feature is not supported in this browser.');
			}
	    $(".lightgallery").lightGallery(); 
	    $(".ui-selectonemenu-label").attr("style","font-family: \'Myanmar3\' !important;font-size: 14px !important;");
    	if($.cookie("lang") == "zawgyi"){
    		$(".ui-inputtext").attr("style","font-family: \'Zawgyi-One\' !important;font-size: 11px !important;");
    		$(".ui-autocomplete-input").attr("style","font-family: \'Myanmar3\' !important;font-size: 14px !important;");
    	}
    	else {
    		$(".ui-inputtext,.ui-autocomplete-input").attr("style","font-family: \'Myanmar3\' !important;font-size: 14px !important;");
    	}
        $(".unilang").click(function(e){
        	$(".ui-inputtext,.ui-autocomplete-input").attr("style","font-family: \'Myanmar3\' !important;font-size: 14px !important;");
        });
        $(".zawgyilang").click(function(e){
        	$(".ui-inputtext,.ui-autocomplete-input").attr("style","font-family: \'Zawgyi-One\' !important;font-size: 11px !important;");
        });
});

