function getBaseUrl() {
    var names = window.location.href.toString().split("/");
    return "ws://" + names[2] + "/" + names[3] + "/interestRate";
}

var websocket = new WebSocket(getBaseUrl());

websocket.onmessage = function(message) { 
	
	var data = JSON.parse(message.data);
	var partyId = "#partyID" + data.partyId;
	
	console.log($(partyId).find(".partyViewCount").text());

	$(partyId).find(".partyViewCount").text(data.party);
	$(partyId).find(".policyDownloadCount").html(data.policy);
	$(partyId).find(".candidateViewCount").html(data.candidate);
};
