function getBaseUrl() {
    var names = window.location.href.toString().split("/");
    return "ws://" + names[2] + "/" + names[3] + "/interestRate";
}

var websocket = new WebSocket(getBaseUrl());

websocket.onmessage = function(message) { 
	
	var data = JSON.parse(message.data);
	var candID = "#candID" + data.cId;
	
	$(candID).find(".candidateInterest").html(data.cInterest);
};
