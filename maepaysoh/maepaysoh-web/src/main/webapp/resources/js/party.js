function getBaseUrl() {
    var names = window.location.href.toString().split("/");
    return "ws://" + names[2] + "/" + names[3] + "/interestRate";
}

var websocket = new WebSocket(getBaseUrl());

websocket.onmessage = function(message) { 
	
	var data = JSON.parse(message.data);
	var partyId = "#partyID" + data.partyId;
	
	$(partyId).find(".partyViewCount").text(data.party);
	$(partyId).find(".policyDownloadCount").text(data.policy);
	$(partyId).find(".candidateViewCount").text(data.candidate);
};

websocket.onopen = function(event) {
	var urlArray = document.URL.split("partyId=");
	websocket.send("refreshParty:"+urlArray[1]);
};

$("#policyDownload").click(function() {
	var partyId = $("#wsPartyId").val();
	websocket.send("policy:"+ partyId);
});