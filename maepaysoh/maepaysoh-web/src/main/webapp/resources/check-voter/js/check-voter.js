$(document).ready(function() {
	 bindFonts();
});
function bindFonts(){
	if($.cookie("lang") == "zawgyi"){
		$(".ui-selectonemenu-items").each(function(){
			$(this).children(".ui-selectonemenu-item").attr("style","font-family: \'Zawgyi-One\' !important;font-size: 13px !important;");
		});
		$(".ui-inputfield").attr("style","font-family: \'Zawgyi-One\' !important;font-size: 13px !important;");
	}	
	else {
		$(".ui-selectonemenu-items").each(function(){
			$(this).children(".ui-selectonemenu-item").attr("style","font-family: \'Myanmar3\' !important;font-size: 13px !important;");
		});
		$(".ui-inputfield").attr("style","font-family: \'Myanmar3\' !important;font-size: 14px !important;");
	}
}

