$(document).ready(function(e){
	var lastDay = new Date(2015,11-1,08);
	var today = new Date();
	if(lastDay > today) {
		var clock = $('#countDown').FlipClock(lastDay,{
			clockFace: 'DailyCounter',
			countdown: true,
			showSeconds: true,
			   callbacks: {
				   stop: function () {
			        	$("#clockMessage").text("သင်၏ ဆန္ဒမဲကိုပေးခြင်းဖြင့် နိုင်ငံသားတစ်ဦး၏ တာဝန်ကို ကျေပွန်ပါ။");
			        	$("#countDown").remove();
			        	$("#countDownPanel").html("<img src='/maepaysoh-web/javax.faces.resource/images/banner/banner-4.jpg.xhtml?ln=base' style='height:200px;width: 100%'/>");
			        }
			    }
		});
	}
	else {
    	$("#clockMessage").text("သင်၏ ဆန္ဒမဲကိုပေးခြင်းဖြင့် နိုင်ငံသားတစ်ဦး၏ တာဝန်ကို ကျေပွန်ပါ။");
    	$("#countDown").remove();
    	$("#countDownPanel").html("<img src='/maepaysoh-web/javax.faces.resource/images/banner/banner-4.jpg.xhtml?ln=base' style='height:200px;width: 100%'/>");
	}
	$(window).on('resize', function(){
	      var win = $(this); //this = window
	      if (win.width() <= 850) {
	    	  $("#countDown").css('display', 'none');
	      }
	      else {
	    	  $("#countDown").css('display', 'inline-block');
	      }
	});
});
