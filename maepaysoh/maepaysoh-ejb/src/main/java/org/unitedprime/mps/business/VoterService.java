package org.unitedprime.mps.business;

import java.util.Date;

import javax.ejb.Local;

import org.unitedprime.vote.Voter;

@Local
public interface VoterService {
    Voter check(String name, Date dob);

    Voter check(String name, String nrc);
}
