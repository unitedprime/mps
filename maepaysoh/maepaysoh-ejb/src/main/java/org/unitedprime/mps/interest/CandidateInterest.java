package org.unitedprime.mps.interest;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class CandidateInterest implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	private int interest;
	
	@ManyToOne
	private Interest partyInterest;
	
	public Interest getPartyInterest() {
		return partyInterest;
	}
	public void setPartyInterest(Interest partyInterest) {
		this.partyInterest = partyInterest;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getInterest() {
		return interest;
	}
	public void setInterest(int interest) {
		this.interest = interest;
	}
	
	public void interest() {
		interest ++;
	}

	
}
