package org.unitedprime.mps.interest;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.Party;
import org.unitedprime.mps.utils.Loggable;

@Loggable
@Stateless
@LocalBean
public class InterestService {

	@Inject
	private Event<Interest> partyInterestEvent;
	@Inject
	private Event<CandidateInterest> candidateInterestEvent;

	@PersistenceContext
	private EntityManager em;

	public void partyInterest(@Observes Party party) {
		Interest interest = em.find(Interest.class, party.getId());

		if (null == interest) {
			interest = new Interest();
			interest.setPartyId(party.getId());
			interest.interestParty();
			em.persist(interest);
		} else {
			interest.interestParty();
			em.merge(interest);
		}

		partyInterestEvent.fire(interest);
	}

	public void candidateInterest(@Observes Candidate candidate) {

		CandidateInterest interest = em.find(CandidateInterest.class,
				candidate.getId());

		if (null == interest) {
			interest = new CandidateInterest();
			interest.setId(candidate.getId());
			interest.interest();

			if (null != candidate.getPartyId()) {
				Interest partyInterest = em.find(Interest.class,
						Integer.parseInt(candidate.getPartyId()));

				if (null == partyInterest) {
					partyInterest = new Interest();
					partyInterest.setPartyId(Integer.parseInt(candidate
							.getPartyId()));
					partyInterest.interestCandidate();
					em.persist(partyInterest);

				} else {
					partyInterest.interestCandidate();
					em.merge(partyInterest);
				}
				interest.setPartyInterest(partyInterest);
			}
			em.persist(interest);
		} else {

			interest.getPartyInterest().interestCandidate();
			em.merge(interest.getPartyInterest());

			interest.interest();
			em.merge(interest);
		}
		candidateInterestEvent.fire(interest);
	}

	public void doRecieveMessage(@Observes WsEvent event) {

		switch (event.getType()) {
		case RefreshParty:
			Interest partyInterest = em.find(Interest.class, event.getPartyId());
			partyInterestEvent.fire(partyInterest);
			
			break;
		case RefreshCandidate:
			CandidateInterest candInterest = em.find(CandidateInterest.class,
					event.getCandidateId());
			candidateInterestEvent.fire(candInterest);
			
			break;
		case ViewPolicy:
			Interest interest = em.find(Interest.class, event.getPartyId());

			if (null == interest) {
				interest = new Interest();
				interest.setPartyId(event.getPartyId());
				interest.interestPolicy();
				em.persist(interest);
			} else {
				interest.interestPolicy();
				em.merge(interest);
			}
			partyInterestEvent.fire(interest);
			break;

		default:
			break;
		}

	}
}
