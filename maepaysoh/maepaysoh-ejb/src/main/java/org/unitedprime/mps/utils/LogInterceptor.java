package org.unitedprime.mps.utils;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

@Loggable
@Interceptor
public class LogInterceptor {

	public LogInterceptor() {
		// TODO Auto-generated constructor stub
	}
	
	@Inject
	private Logger logger;

	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		
		try {
			logger.info(getTarget(ic) + " : Start ");
			return ic.proceed();
		} catch (Exception e) {
			logger.error(getTarget(ic) + " : " + e.getMessage(), e);
		} finally {
			logger.info(getTarget(ic) + " : END ");
		}
		return null;
	}
	
	
	private String getTarget(InvocationContext ic) {
		return ic.getTarget().getClass().getName() + "#" + ic.getMethod().getName();
	}

}
