package org.unitedprime.mps.cach.imp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.unitedprime.mps.api.FaqResources.Param;
import org.unitedprime.mps.business.FaqService;
import org.unitedprime.mps.cach.FaqCachService;
import org.unitedprime.mps.model.Faq;
import org.unitedprime.mps.model.Faqs;
import org.unitedprime.mps.model.Metadata;
import org.unitedprime.mps.model.Pagination;
import org.unitedprime.mps.utils.Loggable;

@Loggable
@Singleton
@LocalBean
public class FaqCachServiceImp implements FaqCachService {

	private List<Faq> repo = Collections.synchronizedList(new ArrayList<>());
	private List<SrcOrLaw> sources = Collections.synchronizedList(new ArrayList<>());

	@Inject
	private FaqService service;

	@Override
	public Faqs find(Predicate<Faq> searchCriteria, int page, int perPage) {

		List<Faq> list = repo.stream().filter(searchCriteria)
				.collect(Collectors.toList());

		Faqs result = new Faqs();
		result.setData(list.stream().skip((page - 1) * perPage).limit(perPage)
				.collect(Collectors.toList()));
		result.setMeta(new Metadata());
		result.getMeta().setPagination(new Pagination());
		result.getMeta().getPagination().setTotal(list.size());
		result.getMeta().getPagination().setCount(result.getData().size());
		result.getMeta().getPagination().setCurrentPage(page);
		result.getMeta().getPagination().setPerPage(perPage);

		return result;
	}

	@Override
	public Faq find(Object id) {
		return this.repo.stream().filter(a -> a.getId().equals(id)).findAny()
				.orElse(null);
	}

	@PostConstruct
	private void init() {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.per_page, 200);
		repo.addAll(service.find(param).getData());

		Set<String> set = new TreeSet<String>();
		repo.stream().map(a -> a.getLawOrSource())
				.filter(a -> !a.isEmpty())
				.forEach(a -> {
					set.add(a);
				});

		SrcOrLaw s = new SrcOrLaw();
		s.setValue("မေးခွန်းအမျိုးအစားအားလုံး");
		s.setKey("");
		this.sources.add(0, s);
		
		set.stream().forEach(a -> {
			SrcOrLaw sl = new SrcOrLaw();
			sl.setKey(a);
			sl.setValue((a.length() > 60)? a.substring(0, 58) + "..." : a);
			sources.add(sl);
		});
	}
	
	@Named
	@Produces
	public List<SrcOrLaw> getSources() {
		return this.sources;
	}

}
