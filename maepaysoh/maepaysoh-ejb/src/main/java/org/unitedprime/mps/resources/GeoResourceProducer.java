package org.unitedprime.mps.resources;

import javax.enterprise.inject.Produces;

import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;

public class GeoResourceProducer {
	
	@Produces
	@GeoResource(GeoType.Township)
	public GeoResources getTownshipResource() {
		return GeoResources.getInstance(GeoType.Township);
	}
	
	@Produces
	@GeoResource(GeoType.District)
	public GeoResources getDistrictResource() {
		return GeoResources.getInstance(GeoType.District);
	}
	
	@Produces
	@GeoResource(GeoType.State)
	public GeoResources getStateResource() {
		return GeoResources.getInstance(GeoType.State);
	}

}
