package org.unitedprime.mps.cach.imp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.unitedprime.mps.api.PartyResources.Param;
import org.unitedprime.mps.business.CandidateService;
import org.unitedprime.mps.business.PartyService;
import org.unitedprime.mps.cach.PartyCachService;
import org.unitedprime.mps.interest.Interest;
import org.unitedprime.mps.interest.InterestRepo;
import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.Party;
import org.unitedprime.mps.model.PartyMetadata;
import org.unitedprime.mps.utils.Loggable;

@Loggable
@Singleton
@LocalBean
public class PartyCachServiceImp implements PartyCachService {

	public PartyCachServiceImp() {
	}

	private List<Party> allParty = Collections
			.synchronizedList(new ArrayList<>());
	@Inject
	private CandidateService candService;
	@Inject
	private InterestRepo interestRepo;

	@Inject
	private PartyService partyService;

	@PostConstruct
	public void init() {

		Map<Param, Object> paramMap = Collections
				.synchronizedMap(new HashMap<>());
		paramMap.put(Param.per_page, 200);

		Parties parties = partyService.findAll(paramMap);
		if (null != parties) {

			parties.getData().stream().forEach(a -> {
				allParty.add(a);
			});
		}
	}

	public Party findById(int id) {
		Party party = allParty.stream().filter(a -> a.getId() == id).findAny()
				.orElse(null);
		
		if(null != party) {
			try {
				Interest inter = interestRepo.getInterestByParty(party.getId());
				
				if (party.getLegistCount() == null) {
					party.setLegistCount(candService.findCandidateCount(party.getId()));
				}

				party.setInterestRate(inter.getInterestRate());
				return party;
			} catch (Exception e) {
			}
			
		}
		return null;
	}

	public Parties find(Predicate<Party> searchCriteria, int perPage, int page) {

		List<Party> list = find(searchCriteria);

		PartyMetadata meta = new PartyMetadata();
		meta.setCount(list.size());
		meta.setCurrent_page(page);
		meta.setPer_page(perPage);

		Parties parties = new Parties();
		parties.setData(new ArrayList<Party>());
		parties.setMeta(meta);

		List<Party> data = list.stream().skip((page - 1) * perPage)
				.limit(perPage).collect(Collectors.toList());

		data.forEach(a -> {
			if (a.getLegistCount() == null) {
				a.setLegistCount(candService.findCandidateCount(a.getId()));
			}
			parties.getData().add(a);
		});

		parties.getData().forEach(a -> {
			try {
				Interest inter = interestRepo.getInterestByParty(a.getId());
				a.setInterestRate(inter.getInterestRate());
			} catch (Exception e) {
			}
		});
		return parties;
	}

	private List<Party> find(Predicate<Party> searchCriteria) {
		return allParty.stream().filter(searchCriteria)
				.collect(Collectors.toList());
	}
	
	@Produces
	public List<String> getPartyNameList() {
		return allParty.stream().map(a -> a.getName()).collect(Collectors.toList());
	}
}
