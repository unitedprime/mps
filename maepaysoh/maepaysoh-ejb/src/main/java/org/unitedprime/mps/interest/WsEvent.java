package org.unitedprime.mps.interest;

import java.io.Serializable;

public class WsEvent implements Serializable{
	
	public enum Type {RefreshParty, RefreshCandidate, ViewPolicy}
	
	private static final long serialVersionUID = 1L;

	private int partyId;
	private String candidateId;
	private Type type;
	
	public Type getType() {
		return type;
	}

	public String getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getPartyId() {
		return partyId;
	}

	public void setPartyId(int partyId) {
		this.partyId = partyId;
	}
	
	
}
