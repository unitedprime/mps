package org.unitedprime.mps.interest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.unitedprime.mps.model.Party.InterestRate;

@Entity
public class Interest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int partyId;
    private int partyInterest;
    private int candidateInterest;
    private int policyInterest;

    public int getPartyId() {
	return partyId;
    }

    public void setPartyId(int partyId) {
	this.partyId = partyId;
    }

    public int getPartyInterest() {
	return partyInterest;
    }

    public void setPartyInterest(int partyInterest) {
	this.partyInterest = partyInterest;
    }

    public int getCandidateInterest() {
	return candidateInterest;
    }

    public void setCandidateInterest(int candidateInterest) {
	this.candidateInterest = candidateInterest;
    }

    public int getPolicyInterest() {
	return policyInterest;
    }

    public void setPolicyInterest(int policyInterest) {
	this.policyInterest = policyInterest;
    }

    public void interestParty() {
	partyInterest++;
    }

    public void interestPolicy() {
	policyInterest++;
    }

    public void interestCandidate() {
	candidateInterest++;
    }

    public Map<String, Integer> getInterestRate() {
	Map<String, Integer> map = new HashMap<>();
	map.put(InterestRate.PARTY.toString(), partyInterest);
	map.put(InterestRate.CANDIDATE.toString(), candidateInterest);
	map.put(InterestRate.POLICY.toString(), policyInterest);
	return map;
    }
}
