package org.unitedprime.mps.business.imp;

import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.unitedprime.mps.api.PartyResources;
import org.unitedprime.mps.api.PartyResources.Param;
import org.unitedprime.mps.business.PartyService;
import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.Party;
import org.unitedprime.mps.utils.Loggable;

@Loggable
@Stateless
@LocalBean
public class PartyServiceImpl implements PartyService {

	@Inject
	private PartyResources resource;

	@Override
	public Party findById(int id) {
		return resource.findById(id).getParty();
	}

	@Override
	public Parties findAll(Map<Param, Object> paramMap) {
		return resource.findAll(paramMap);
	}

}
