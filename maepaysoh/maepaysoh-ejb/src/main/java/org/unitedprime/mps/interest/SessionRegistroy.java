package org.unitedprime.mps.interest;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.websocket.Session;

import org.unitedprime.mps.utils.Loggable;

@Loggable
@Singleton
@LocalBean
public class SessionRegistroy {

    public SessionRegistroy() {
    }
    
	private final Set<Session> sessions = Collections.synchronizedSet(new HashSet<>());
	
	@Lock(LockType.READ)
	public Set<Session> getAll() {
		return sessions;
	}
	
	@Lock(LockType.WRITE)
	public void add(Session session) {
		sessions.add(session);
	}
	
	@Lock(LockType.WRITE)
	public void remove(Session session) {
		sessions.remove(session);
	}

}
