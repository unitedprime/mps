package org.unitedprime.mps.cach;

import java.util.function.Predicate;

import javax.ejb.Local;

import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.Party;

@Local
public interface PartyCachService {

	Parties find(Predicate<Party> searchCriteria, int perPage, int page);
	Party findById(int id);
}
