package org.unitedprime.mps.business;

import java.util.Map;

import javax.ejb.Local;

import org.unitedprime.mps.api.FaqResources.Param;
import org.unitedprime.mps.model.Faq;
import org.unitedprime.mps.model.Faqs;

@Local
public interface FaqService {
    Faq findById(String id);

    Faqs find(Map<Param, Object> paramMap);
}
