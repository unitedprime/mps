package org.unitedprime.mps.interest;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.unitedprime.mps.utils.Loggable;

@Loggable
@Local
@Stateless
public class InterestRepoBean implements InterestRepo {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Interest getInterestByParty(int partyId) {
	Interest inter = em.find(Interest.class, partyId);
	if (inter == null) {
	    inter = new Interest();
	}
	return inter;
    }

    @Override
    public CandidateInterest getInterestByCandidate(String candidateId) {
	CandidateInterest inter = em.find(CandidateInterest.class, candidateId);
	if (inter == null) {
	    inter = new CandidateInterest();
	}
	return inter;

    }

}
