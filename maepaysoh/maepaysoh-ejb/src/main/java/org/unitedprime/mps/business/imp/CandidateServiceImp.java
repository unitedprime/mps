package org.unitedprime.mps.business.imp;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.unitedprime.mps.api.CandidateResources;
import org.unitedprime.mps.api.CandidateResources.Param;
import org.unitedprime.mps.api.Legislature;
import org.unitedprime.mps.business.CandidateService;
import org.unitedprime.mps.cach.PartyCachService;
import org.unitedprime.mps.interest.InterestRepo;
import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.Candidates;
import org.unitedprime.mps.utils.Loggable;
import org.unitedprime.omi.api.MotionResource;
import org.unitedprime.omi.api.QuestionResource;
import org.unitedprime.omi.model.MotionsByCandidate;
import org.unitedprime.omi.model.QuestionsByCancidate;

@Loggable
@Stateless
@LocalBean
public class CandidateServiceImp implements CandidateService {

	@Inject
	private CandidateResources candiResource;
	@Inject
	private InterestRepo interestService;
	@Inject
	private PartyCachService partyService;
	@Inject
	private MotionResource motionResource;
	@Inject
	private QuestionResource questionResource;
	
	@Override
	public Candidate findById(String id) {
		Candidate candidate = candiResource.findById(id).getData();
		
		try {
			if(null != candidate 
					&& null != candidate.getPartyId() 
					&& !candidate.getPartyId().isEmpty()) {
				candidate.setParty(partyService.findById(Integer.parseInt(candidate.getPartyId())));
			}
		} catch(Exception e) {
			System.err.println(e.getCause());
		}
		
		try {
			if(null != candidate 
					&& null != candidate.getMpid() 
					&& !candidate.getMpid().isEmpty()) {

				MotionsByCandidate motions = motionResource.findByCandidate(candidate.getMpid());
				if(null != motions) {
					candidate.setMotions(motions.getMotions());
				}
				
				QuestionsByCancidate questions = questionResource.findByCandidate(candidate.getMpid());
				if(null != motions) {
					candidate.setQuestions(questions.getQuestions());
				}
			}
		} catch(Exception e) {
			System.err.println(e.getCause());
		}
		
		return candidate;
	}

	@Override
	public Candidates findByName(String name) {
		return this.setInterest(candiResource.findByName(name));
	}

	@Override
	public Candidates getAllCandidates(Map<Param, Object> param) {

		if (param.keySet().contains(Param.q)) {
			return candiResource.findByName((String) param.get(Param.q));
		} else {
			return this.setInterest(candiResource.find(param));
		}
	}

	@Override
	public Candidates findByTownship(String tsPcode, int perPage, int page) {

		Map<Param, Object> param = new HashMap<>();
		param.put(Param.constituency_ts_pcode, tsPcode);
		param.put(Param.per_page, perPage);
		param.put(Param.page, page);

		return this.setInterest(candiResource.find(param));
	}

	@Override
	public Candidates findByParty(int partyId, int perPage, int page) {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.party, partyId);
		param.put(Param.per_page, perPage);
		param.put(Param.page, page);

		return this.setInterest(candiResource.find(param));
	}

	@Override
	public Candidates findByPartyAndLegis(int partyId, String legis,
			int perPage, int page) {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.party, partyId);
		param.put(Param.per_page, perPage);
		param.put(Param.page, page);
		param.put(Param.legislature, legis);

		return this.setInterest(candiResource.find(param));
	}

	@Override
	public Map<String, Integer> findCandidateCount() {
		Map<String, Integer> result = new HashMap<>();

		for (Legislature l : Legislature.values()) {
			result.put(l.toString(), getCandidateCountByLegis(l));
		}

		return result;
	}

	@Override
	public Map<String, Integer> findCandidateCount(int partyId) {
		Map<String, Integer> result = new HashMap<>();

		for (Legislature l : Legislature.values()) {
			result.put(l.toString(), getCandidateCountByLegis(l, partyId));
		}

		return result;
	}

	private int getCandidateCountByLegis(Legislature legis) {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.per_page, 1);
		param.put(Param.legislature, legis.toString());

		Candidates result = candiResource.find(param);
		return result.getMeta().getPagination().getTotal();
	}

	private int getCandidateCountByLegis(Legislature legis, int partyId) {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.per_page, 1);
		param.put(Param.party, partyId);
		param.put(Param.legislature, legis.toString());

		Candidates result = candiResource.find(param);
		return result.getMeta().getPagination().getTotal();
	}

	private Candidates setInterest(Candidates candidates) {
		try {
			candidates.getData().forEach(
					a -> {
						try {
							a.setInterest(interestService
									.getInterestByCandidate(a.getId())
									.getInterest());
						} catch (Exception e) {
							System.err.println(e.getCause());
						}
					});
		} catch (Exception e) {
			System.err.println(e.getCause());
		}

		return candidates;
	}
}
