package org.unitedprime.mps.interest;

import javax.ejb.Local;

@Local
public interface InterestRepo {
	
	Interest getInterestByParty(int partyId);
	CandidateInterest getInterestByCandidate(String candidateId);

}
