package org.unitedprime.mps.business;

import java.util.Map;

import javax.ejb.Local;

import org.unitedprime.mps.api.PartyResources.Param;
import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.Party;

@Local
public interface PartyService {

    Party findById(int id);

    Parties findAll(Map<Param, Object> paramMap);

}
