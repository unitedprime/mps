package org.unitedprime.mps.business;

import java.util.Map;

import javax.ejb.Local;

import org.unitedprime.mps.api.CandidateResources.Param;
import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.Candidates;

@Local
public interface CandidateService {

    Candidate findById(String id);

    Candidates findByName(String name);

    Candidates getAllCandidates(Map<Param, Object> param);

    Candidates findByTownship(String tsPcode, int perPage, int page);

    Candidates findByParty(int partyId, int perPage, int page);
    
    Candidates findByPartyAndLegis(int partyId, String legis, int perPage, int page);
    
    Map<String, Integer> findCandidateCount();
    
    Map<String, Integer> findCandidateCount(int partyId);
    
}
