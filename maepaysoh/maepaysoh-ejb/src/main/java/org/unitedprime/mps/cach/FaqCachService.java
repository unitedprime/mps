package org.unitedprime.mps.cach;

import java.util.function.Predicate;

import javax.ejb.Local;

import org.unitedprime.mps.model.Faq;
import org.unitedprime.mps.model.Faqs;

@Local
public interface FaqCachService {
	
	Faqs find(Predicate<Faq> searchCriteria, int page, int perPage);
	Faq find(Object id);
}
