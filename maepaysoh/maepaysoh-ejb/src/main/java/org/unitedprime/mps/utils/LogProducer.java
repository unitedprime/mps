package org.unitedprime.mps.utils;

import javax.enterprise.inject.Produces;

import org.apache.log4j.Logger;

public class LogProducer {
	
	@Produces
	public Logger getLogger() {
		return Logger.getLogger("org.unitedprime.maepaysoh");
	}

}
