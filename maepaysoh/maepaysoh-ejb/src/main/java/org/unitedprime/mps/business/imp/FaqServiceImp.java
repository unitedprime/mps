package org.unitedprime.mps.business.imp;

import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.unitedprime.mps.api.FaqResources;
import org.unitedprime.mps.api.FaqResources.Param;
import org.unitedprime.mps.business.FaqService;
import org.unitedprime.mps.model.Faq;
import org.unitedprime.mps.model.Faqs;
import org.unitedprime.mps.utils.Loggable;

@Loggable
@Stateless
@LocalBean
public class FaqServiceImp implements FaqService {

	@Inject
	private FaqResources resource;

	@Override
	public Faq findById(String id) {
		return resource.findById(id).getData();
	}

	@Override
	public Faqs find(Map<Param, Object> paramMap) {
		
		if(paramMap.keySet().contains(Param.q)) {
			return resource.findByKeyword(paramMap);
		} else {
			return resource.find(paramMap);
		}
	}

}
