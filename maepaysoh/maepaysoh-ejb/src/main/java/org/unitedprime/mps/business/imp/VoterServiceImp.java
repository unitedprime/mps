package org.unitedprime.mps.business.imp;

import java.util.Date;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.unitedprime.mps.business.VoterService;
import org.unitedprime.mps.utils.Loggable;
import org.unitedprime.vote.Voter;
import org.unitedprime.vote.VoterResource;

@Loggable
@Stateless
@LocalBean
public class VoterServiceImp implements VoterService {
	
	@Inject
	private VoterResource resource;

	@Override
	public Voter check(String name, Date dob) {
		return resource.check(name, dob);
	}

	@Override
	public Voter check(String name, String nrc) {
		return resource.check(name, nrc);
	}

}
