                <datasource jta="true" jndi-name="java:jboss/datasources/maepaysohDS" pool-name="MaepaysohDS" enabled="true" use-ccm="true">
                    <connection-url>jdbc:mysql://127.0.0.1:3306/mps_db</connection-url>
                    <driver-class>com.mysql.jdbc.Driver</driver-class>
                    <driver>mysql-connector-java-5.1.34.jar_com.mysql.jdbc.Driver_5_1</driver>
                    <security>
                        <user-name>mpsusr</user-name>
                        <password>mps@pwd</password>
                    </security>
                    <validation>
                        <validate-on-match>false</validate-on-match>
                        <background-validation>false</background-validation>
                    </validation>
                    <timeout>
                        <set-tx-query-timeout>false</set-tx-query-timeout>
                        <blocking-timeout-millis>0</blocking-timeout-millis>
                        <idle-timeout-minutes>0</idle-timeout-minutes>
                        <query-timeout>0</query-timeout>
                        <use-try-lock>0</use-try-lock>
                        <allocation-retry>0</allocation-retry>
                        <allocation-retry-wait-millis>0</allocation-retry-wait-millis>
                    </timeout>
                    <statement>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
                </datasource>
                
                
                
                
############################################################ db create ##########################################
drop database if exists mps_db;
create database mps_db character set utf8;
create user 'mpsusr'@'%' identified by 'mps@pwd';
grant all privileges on mps_db.* to 'mpsusr'@'%' identified by 'mps@pwd' with grant option;
flush privileges;
