package org.unitedprime.omi.model;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Question implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String house;
	private String session;
	private String date;
	private String issue;
	private String purpose;

	private Map<String, String> source;
	private Map<String, String> description;
	private Map<String, String> respondent;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Map<String, String> getSource() {
		return source;
	}

	public void setSource(Map<String, String> source) {
		this.source = source;
	}

	public Map<String, String> getDescription() {
		return description;
	}

	public void setDescription(Map<String, String> description) {
		this.description = description;
	}

	public Map<String, String> getRespondent() {
		return respondent;
	}

	public void setRespondent(Map<String, String> respondent) {
		this.respondent = respondent;
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
