package org.unitedprime.omi.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonAnySetter;

@SuppressWarnings("unchecked")
public class Motion implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String house;
	private String session;
	private String issue;
	private String purpose;
	private String status;
	private Map<String, String> source;
	private Map<String, String> description;
	private Map<String, String> respondent;
	@XmlElement(name="submitted_date")
	private Date submittedDate;
	@XmlElement(name="response_date")
	private Date responseDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<String, String> getSource() {
		return source;
	}

	public void setSource(Object source) {
		try {
			this.source = (Map<String, String>) source;
		} catch (ClassCastException e) {
			this.source = Collections.emptyMap();
		}
	}

	public Map<String, String> getDescription() {
		return description;
	}

	public void setDescription(Object description) {
		try {
			this.description = (Map<String, String>) description;
		} catch (ClassCastException e) {
			this.description = Collections.emptyMap();
		}
	}

	public Map<String, String> getRespondent() {
		return respondent;
	}

	public void setRespondent(Object respondent) {
		try {
			this.respondent = (Map<String, String>) respondent;
		} catch (ClassCastException e) {
			this.respondent = Collections.emptyMap();
		}
	}

	public Date getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Object dt) {
		try {
			String str = dt.toString();
			DateFormat df = new SimpleDateFormat("dd-MMM-yy");
			this.submittedDate = df.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Date getResponseDate() {
		return responseDate;
	}

	public void setResponseDate(Object dt) {
		try {
			String str = dt.toString();
			DateFormat df = new SimpleDateFormat("dd-MMM-yy");
			this.responseDate = df.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
