package org.unitedprime.omi.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class MotionsByCandidate implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private Map<String, String> name;
	private List<Motion> motions = Collections.emptyList();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, String> getName() {
		return name;
	}

	public void setName(Map<String, String> name) {
		this.name = name;
	}

	public List<Motion> getMotions() {
		return motions;
	}

	public void setMotions(List<Motion> motions) {
		this.motions = motions;
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
