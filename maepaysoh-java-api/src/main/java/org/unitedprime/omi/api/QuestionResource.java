package org.unitedprime.omi.api;

import java.io.Serializable;

import org.unitedprime.omi.model.Question;
import org.unitedprime.omi.model.QuestionsByCancidate;

public interface QuestionResource extends Serializable{

	Question findById(String id);
	QuestionsByCancidate findByCandidate(String mpid);

}
