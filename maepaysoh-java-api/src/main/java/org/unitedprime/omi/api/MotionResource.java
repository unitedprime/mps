package org.unitedprime.omi.api;

import java.io.Serializable;

import org.unitedprime.omi.model.Motion;
import org.unitedprime.omi.model.MotionsByCandidate;

public interface MotionResource extends Serializable{

	Motion findById(String id);
	MotionsByCandidate findByCandidate(String mpid);

}
