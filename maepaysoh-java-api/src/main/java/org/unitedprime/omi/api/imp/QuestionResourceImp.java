package org.unitedprime.omi.api.imp;

import org.unitedprime.mps.api.imp.RestClient;
import org.unitedprime.omi.api.QuestionResource;
import org.unitedprime.omi.model.Question;
import org.unitedprime.omi.model.QuestionsByCancidate;

public class QuestionResourceImp extends RestClient implements QuestionResource {

	private static final long serialVersionUID = 1L;

	@Override
	protected void setBaseResource() {
		setResource("parliament/questions");
	}

	@Override
	public Question findById(String id) {
		return getTarget(id).request().get(Question.class);
	}

	@Override
	public QuestionsByCancidate findByCandidate(String mpid) {
		return getTarget(mpid).request()
				.get(QuestionsByCancidate.class);
	}

}
