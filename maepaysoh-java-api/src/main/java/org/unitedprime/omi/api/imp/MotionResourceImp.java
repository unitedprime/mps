package org.unitedprime.omi.api.imp;

import org.unitedprime.mps.api.imp.RestClient;
import org.unitedprime.omi.api.MotionResource;
import org.unitedprime.omi.model.Motion;
import org.unitedprime.omi.model.MotionsByCandidate;

public class MotionResourceImp extends RestClient implements MotionResource{

	private static final long serialVersionUID = 1L;

	@Override
	public Motion findById(String id) {
		return getTarget(id).request().get(Motion.class);
	}

	@Override
	public MotionsByCandidate findByCandidate(String mpid) {
		return getTarget(mpid).request().get(MotionsByCandidate.class);
	}
	

	@Override
	protected void setBaseResource() {
		setResource("parliament/motions");
	}

}
