package org.unitedprime.mps.api.imp;

import java.util.Map;

import javax.ws.rs.client.WebTarget;

import org.unitedprime.mps.api.CandidateResources;
import org.unitedprime.mps.model.CandidateSingleResult;
import org.unitedprime.mps.model.Candidates;

public class CandidateResourcesImp extends RestClient implements CandidateResources {

	private static final long serialVersionUID = 1L;

	@Override
	public CandidateSingleResult findById(String id) {
		return getTarget().path(id).request().get(CandidateSingleResult.class);
	}

	@Override
	public Candidates find(Map<Param, Object> paramMap) {
		
		class Container {
			WebTarget target;
		}
		
		Container c = new Container();
		c.target = getTarget().path("list");
		
		if(null != paramMap)
			paramMap.forEach((a,b) -> c.target = c.target.queryParam(a.toString(), b));
		
		return c.target.request().get(Candidates.class);
	}

	@Override
	public Candidates findByName(String name) {
		WebTarget target = getTarget().path("search").queryParam("q", name);
		return target.request().get(Candidates.class);
	}

	@Override
	protected void setBaseResource() {
		setResource("candidate");
	}
	

}
