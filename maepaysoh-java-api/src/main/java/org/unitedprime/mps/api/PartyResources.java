package org.unitedprime.mps.api;

import java.io.Serializable;
import java.util.Map;

import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.PartySingleResult;

public interface PartyResources extends Serializable {

    public enum Param {
	per_page, page;
    }

    Parties findAll(Map<Param, Object> paramMap);

    PartySingleResult findById(int id);
}
