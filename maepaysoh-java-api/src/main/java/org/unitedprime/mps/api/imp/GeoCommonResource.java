package org.unitedprime.mps.api.imp;

import java.util.Map;

import javax.ws.rs.client.WebTarget;

import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.model.GeoListResult;
import org.unitedprime.mps.model.GeoSingleResult;

public abstract class GeoCommonResource extends RestClient implements
		GeoResources {

	private static final long serialVersionUID = 1L;

	@Override
	public GeoListResult find() {
		return getTarget().queryParam("no_geo", true).request()
				.get(GeoListResult.class);
	}

	@Override
	public GeoListResult find(Map<Param, Object> params) {
		class Container {
			WebTarget target;
		}

		Container c = new Container();
		c.target = getTarget();

		if (null != params)
			params.forEach((a, b) -> c.target = c.target.queryParam(
					a.toString(), b));

		return c.target.queryParam("no_geo", true).request()
				.get(GeoListResult.class);
	}

	@Override
	public GeoSingleResult find(String id) {
		return getTarget().path(id).request().get(GeoSingleResult.class);
	}

	@Override
	public GeoListResult find(double log, double lat) {
		return getTarget().path("find").queryParam("no_geo", true)
				.queryParam("lat", lat).queryParam("lon", log).request()
				.get(GeoListResult.class);
	}

}
