package org.unitedprime.mps.api;

import java.io.Serializable;
import java.util.Map;

import org.unitedprime.mps.model.CandidateSingleResult;
import org.unitedprime.mps.model.Candidates;

public interface CandidateResources extends Serializable{

	public enum Gender {
		F("Female"), M("Male");
		
		private String value;
		
		private Gender(String v) {
			value = v;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	public enum Param {
		_with ,token ,font ,per_page, page,gender ,legislature ,party ,constituency_st_pcode ,constituency_dt_pcode , constituency_am_pcode, constituency_ts_pcode ,q;
	}
	
	CandidateSingleResult findById(String id);
	Candidates find(Map<Param, Object> paramMap);
	Candidates findByName(String name);
	
}
