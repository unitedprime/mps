package org.unitedprime.mps.api;

import java.io.Serializable;
import java.util.Map;

import org.unitedprime.mps.api.imp.GeoDistrictResource;
import org.unitedprime.mps.api.imp.GeoStateResource;
import org.unitedprime.mps.api.imp.GeoTownshipResource;
import org.unitedprime.mps.model.GeoListResult;
import org.unitedprime.mps.model.GeoSingleResult;

public interface GeoResources extends Serializable {

    public enum Param {
	no_geo, per_page, page, st_name, dt_name, ts_name, object_id, st_pcode, dt_pcode, ts_pcode, by_party
    }

    public enum GeoType {
	State, District, Township;
    }

    GeoListResult find();

    GeoListResult find(Map<Param, Object> params);

    GeoSingleResult find(String id);

    GeoListResult find(double log, double lat);

    public static GeoResources getInstance(GeoType type) {
	GeoResources resource = null;
	switch (type) {
	case State:
	    resource = new GeoStateResource();
	    break;
	case District:
	    resource = new GeoDistrictResource();
	    break;
	case Township:
	    resource = new GeoTownshipResource();
	    break;

	default:
	    break;
	}
	return resource;
    }

}
