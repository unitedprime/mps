package org.unitedprime.mps.api;

import java.io.Serializable;
import java.util.Map;

import org.unitedprime.mps.model.FaqSingleResult;
import org.unitedprime.mps.model.Faqs;

public interface FaqResources extends Serializable{
	
	public enum Param {
		 token ,type ,section ,per_page ,page ,q 
	}

	FaqSingleResult findById(String id);
	Faqs find(Map<Param, Object> paramMap);
	Faqs findByKeyword(Map<Param, Object> paramMap);
	
}
