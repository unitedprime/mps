package org.unitedprime.mps.api.imp;

import java.io.IOException;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public abstract class RestClient {

	private String token;

	private static String BASE_URL = "http://api.maepaysoh.org";

	private String baseResource;
	private Client client;

	protected RestClient() {
		try {
			client = ClientBuilder.newClient();
			setBaseResource();
			Properties props = new Properties();
			props.load(this.getClass().getClassLoader()
					.getResourceAsStream("api.properties"));
			token = props.getProperty("key");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected abstract void setBaseResource();

	protected void setResource(String resource) {
		this.baseResource = String.format("%s/%s", BASE_URL, resource);
	}

	protected WebTarget getTarget() {
		return client.target(this.baseResource).queryParam("token", token);
	}

	protected WebTarget getTarget(String omiId) {
		return client.target(this.baseResource)
				.path(String.format("%s.json", omiId))
				.queryParam("token", token);
	}

}
