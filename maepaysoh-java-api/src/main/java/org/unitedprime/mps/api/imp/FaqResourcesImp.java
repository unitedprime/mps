package org.unitedprime.mps.api.imp;

import java.util.Map;

import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;

import org.unitedprime.mps.api.FaqResources;
import org.unitedprime.mps.model.FaqSingleResult;
import org.unitedprime.mps.model.Faqs;

public class FaqResourcesImp extends RestClient implements FaqResources {

	private static final long serialVersionUID = 1L;

	@Override
	public FaqSingleResult findById(String id) {
		Builder builder = getTarget().path(id).request();
		return builder.get(FaqSingleResult.class);
	}

	@Override
	public Faqs find(Map<Param, Object> paramMap) {
		class Container {
			WebTarget target;
		}

		Container c = new Container();
		c.target = getTarget().path("list");

		if (null != paramMap)
			paramMap.forEach((a, b) -> c.target = c.target.queryParam(
					a.toString(), b));
		return c.target.request().get(Faqs.class);
	}

	@Override
	public Faqs findByKeyword(Map<Param, Object> paramMap) {
		class Container {
			WebTarget target;
		}

		Container c = new Container();
		c.target = getTarget().path("search");

		if (null != paramMap)
			paramMap.forEach((a, b) -> c.target = c.target.queryParam(
					a.toString(), b));
		return c.target.request().get(Faqs.class);
	}

	@Override
	protected void setBaseResource() {
		setResource("faq");
	}

}
