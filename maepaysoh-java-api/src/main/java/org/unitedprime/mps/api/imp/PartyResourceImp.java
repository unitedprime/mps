package org.unitedprime.mps.api.imp;

import java.util.Map;

import javax.ws.rs.client.WebTarget;

import org.unitedprime.mps.api.PartyResources;
import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.PartySingleResult;

public class PartyResourceImp extends RestClient implements PartyResources {

	private static final long serialVersionUID = 1L;

	@Override
	public Parties findAll(Map<Param, Object> paramMap) {
		class Container {
			WebTarget target;
		}
		
		Container c = new Container();
		c.target = getTarget();
		
		if(null != paramMap)
			paramMap.forEach((a, b) -> c.target = c.target.queryParam(a.toString(),
				b));

		return c.target.request().get(Parties.class);
	}

	@Override
	public PartySingleResult findById(int id) {
		return getTarget().path(String.valueOf(id)).request()
				.get(PartySingleResult.class);
	}

	@Override
	protected void setBaseResource() {
		setResource("party");
	}

}
