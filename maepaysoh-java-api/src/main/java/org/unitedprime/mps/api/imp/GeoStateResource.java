package org.unitedprime.mps.api.imp;

import org.unitedprime.mps.api.GeoResources;

public class GeoStateResource extends GeoCommonResource implements GeoResources {
    private static final long serialVersionUID = 1L;

    @Override
    protected void setBaseResource() {
	setResource("geo/upperhouse");
    }

}
