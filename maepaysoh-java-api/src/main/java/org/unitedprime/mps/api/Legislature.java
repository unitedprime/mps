package org.unitedprime.mps.api;

public enum Legislature {
	
	UPPER("အမျိုးသားလွှတ်တော်"), LOWER("ပြည်သူ့လွှတ်တော်"), REGION("တိုင်းဒေသကြီး/ပြည်နယ် လွှတ်တော်");
	
	private String value;
	
	private Legislature(String s) {
		this.value = s;
	}
	
	@Override
	public String toString() {
		return this.value;
	}
	
	public static Legislature getLegislature(String value) { 
		switch (value) {
		case "ပြည်သူ့လွှတ်တော်":
			return LOWER;
		case "အမျိုးသားလွှတ်တော်":
			return UPPER;
		case "တိုင်းဒေသကြီး/ပြည်နယ် လွှတ်တော်":
			return REGION;
		default:
			return null;
		}
	}

}
