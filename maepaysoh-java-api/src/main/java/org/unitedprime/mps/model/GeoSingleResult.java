package org.unitedprime.mps.model;

import java.io.Serializable;

public class GeoSingleResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private Geo data;

	public Geo getData() {
		return data;
	}

	public void setData(Geo data) {
		this.data = data;
	}

}
