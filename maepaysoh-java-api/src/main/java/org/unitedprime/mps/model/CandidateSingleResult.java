package org.unitedprime.mps.model;

import java.io.Serializable;

public class CandidateSingleResult implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Candidate data;

	public Candidate getData() {
		return data;
	}

	public void setData(Candidate data) {
		this.data = data;
	}
}
