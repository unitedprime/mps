package org.unitedprime.mps.model;

import java.io.Serializable;

public class FaqSingleResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Faq data;

	public Faq getData() {
		return data;
	}

	public void setData(Faq data) {
		this.data = data;
	}
}
