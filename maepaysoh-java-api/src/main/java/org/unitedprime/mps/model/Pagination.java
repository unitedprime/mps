package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Pagination implements Serializable {

	private static final long serialVersionUID = 1L;

	private int total;
	private int count;
	@XmlElement(name="per_page")
	private int perPage;
	@XmlElement(name="current_page")
	private int currentPage;
	@XmlElement(name="total_pages")
	private int totalPages;

	private List<String> links = new ArrayList<String>();

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getPerPage() {
		return perPage;
	}

	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public List<String> getLinks() {
		return links;
	}

	@SuppressWarnings("unchecked")
	public void setLinks(Object links) {
		if (links instanceof Map) {
			Map<String, Object> map = (Map<String, Object>) links;
			if (null == map.entrySet())
				this.links = Collections.emptyList();
		} else {
			this.links = (List<String>) links;
		}
	}

	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}

}
