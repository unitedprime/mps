package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonAnySetter;


public class Parties implements Serializable {

	private static final long serialVersionUID = 1L;
	@XmlElement(name="_meta")
	private PartyMetadata meta;
	private List<Party> data;

	public PartyMetadata getMeta() {
		return meta;
	}

	public void setMeta(PartyMetadata meta) {
		this.meta = meta;
	}

	public List<Party> getData() {
		return data;
	}

	public void setData(List<Party> data) {
		this.data = (List<Party>) data;
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
