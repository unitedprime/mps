package org.unitedprime.mps.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Constituency implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private int number;
	@XmlElement(name = "ST_PCODE")
	private String stPcode;
	@XmlElement(name = "DT_PCODE")
	private String dtPcode;
	@XmlElement(name = "TS_PCODE")
	private String tsPcode;
	@XmlElement(name = "AM_PCODE")
	private String amPcode;
	private String parent;
	@XmlElement(name = "parent_type")
	private String parentType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public String getAmPcode() {
		return amPcode;
	}

	public void setAmPcode(String amPcode) {
		this.amPcode = amPcode;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getStPcode() {
		return stPcode;
	}

	public void setStPcode(String stPcode) {
		this.stPcode = stPcode;
	}

	public String getDtPcode() {
		return dtPcode;
	}

	public void setDtPcode(String dtPcode) {
		this.dtPcode = dtPcode;
	}

	public String getTsPcode() {
		return tsPcode;
	}

	public void setTsPcode(String tsPcode) {
		this.tsPcode = tsPcode;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getParentType() {
		return parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
