package org.unitedprime.mps.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Faq implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String question;
	private String answer;
	private String category;
	private String number;
	private String type;
	@XmlElement(name="article_or_section")
	private String articleOrSection;
	@XmlElement(name="law_or_source")
	private String lawOrSource;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getArticleOrSection() {
		return articleOrSection;
	}

	public void setArticleOrSection(String articleOrSection) {
		this.articleOrSection = articleOrSection;
	}

	public String getLawOrSource() {
		return lawOrSource;
	}

	public void setLawOrSource(String lawOrSource) {
		this.lawOrSource = lawOrSource;
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
