package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonAnySetter;
import org.unitedprime.mps.api.CandidateResources.Gender;
import org.unitedprime.omi.model.Motion;
import org.unitedprime.omi.model.Question;

public class Candidate implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String mpid;
	private String name;
	private String religion;
	private Gender gender;
	@XmlElement(name="photo_url")
	private String photo;
	private String legislature;
	private long birthdate;
	private String education;
	private String occupation;
	private String ethnicity;
	@XmlElement(name="ward_village")
	private String wordVillage;
	@XmlElement(name="party_id")
	private String partyId;
	private Constituency constituency;
	private Person father;
	private Person mother;
	@XmlTransient
	private int interest;
	
	@XmlTransient
	private Party party;
	@XmlTransient
	private List<Motion> motions;
	@XmlTransient
	private List<Question> questions;

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	public List<Motion> getMotions() {
		return motions;
	}

	public void setMotions(List<Motion> motions) {
		this.motions = motions;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public int getInterest() {
		return interest;
	}

	public void setInterest(int interest) {
		this.interest = interest;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMpid() {
		return mpid;
	}

	public void setMpid(String mpid) {
		this.mpid = mpid;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Object gender) {
		try {
			this.gender = (Gender)gender;
		} catch (Exception e) {

		}
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getLegislature() {
		return legislature;
	}

	public void setLegislature(String legislature) {
		this.legislature = legislature;
	}


	public long getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(long birthdate) {
		this.birthdate = birthdate;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getWordVillage() {
		return wordVillage;
	}

	public void setWordVillage(String wordVillage) {
		this.wordVillage = wordVillage;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public Constituency getConstituency() {
		return constituency;
	}

	public void setConstituency(Constituency constituency) {
		this.constituency = constituency;
	}

	public Person getFather() {
		return father;
	}

	public void setFather(Person father) {
		this.father = father;
	}

	public Person getMother() {
		return mother;
	}

	public void setMother(Person mother) {
		this.mother = mother;
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
