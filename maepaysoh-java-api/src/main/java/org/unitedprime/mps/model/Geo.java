package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Geo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String type;
	private Map<String, String> properties;
	private Map<String, Object> geometry;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	public Map<String, Object> getGeometry() {
		return geometry;
	}

	public void setGeometry(Map<String, Object> geometry) {
		this.geometry = geometry;
	}

	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
