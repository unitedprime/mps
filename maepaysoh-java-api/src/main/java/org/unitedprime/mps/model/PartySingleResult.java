package org.unitedprime.mps.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class PartySingleResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@XmlElement(name="_meta")
	private PartyMetadata meta;
	@XmlElement(name="data")
	private Party party;

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
}
