package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class PartyMetadata implements Serializable {

	private static final long serialVersionUID = 1L;
	private String status;
	private int count;
	private int api_version;
	private boolean unicode;
	private String format;
	private int per_page;
	private int current_page;
	private int total_pages;
	private List<String> links;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getApi_version() {
		return api_version;
	}

	public void setApi_version(int api_version) {
		this.api_version = api_version;
	}

	public boolean isUnicode() {
		return unicode;
	}

	public void setUnicode(boolean unicode) {
		this.unicode = unicode;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getPer_page() {
		return per_page;
	}

	public void setPer_page(int per_page) {
		this.per_page = per_page;
	}

	public int getCurrent_page() {
		return current_page;
	}

	public void setCurrent_page(int current_page) {
		this.current_page = current_page;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

	public List<String> getLinks() {
		return links;
	}

	@SuppressWarnings("unchecked")
	public void setLinks(Object links) {
		if (links instanceof Map) {
			Map<String, Object> map = (Map<String, Object>) links;
			if (null == map.entrySet())
				this.links = Collections.emptyList();
		} else {
			this.links = (List<String>) links;
		}
	}

	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
