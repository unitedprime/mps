package org.unitedprime.mps.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Metadata implements Serializable {

    private static final long serialVersionUID = 1L;

    private String format;
    private Pagination pagination;
    private String data_version;

    public String getFormat() {
	return format;
    }

    public void setFormat(String format) {
	this.format = format;
    }

    public Pagination getPagination() {
	return pagination;
    }

    public void setPagination(Pagination pagination) {
	this.pagination = pagination;
    }

    public String getData_version() {
	return data_version;
    }

    public void setData_version(String data_version) {
	this.data_version = data_version;
    }

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
	System.out.println(key);
    }
}
