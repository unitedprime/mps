package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Candidates implements Serializable {

	private static final long serialVersionUID = 1L;

	private Metadata meta;
	private List<Candidate> data = new ArrayList<Candidate>();

	public Metadata getMeta() {
		return meta;
	}

	public void setMeta(Metadata meta) {
		this.meta = meta;
	}

	public List<Candidate> getData() {
		return data;
	}

	public void setData(List<Candidate> data) {
		this.data = data;
	}
	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
