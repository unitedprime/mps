package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Party implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum InterestRate {
	PARTY, CANDIDATE, POLICY
    }

    private int id;
    @XmlElement(name = "party_name")
    private String name;
    @XmlElement(name = "party_name_english")
    private String eName;
    private String abbreviation;
    @XmlElement(name = "establishment_date")
    private long estDate;
    @XmlElement(name = "member_count")
    private String memberCount;
    @XmlElement(name = "establishment_approval_date")
    private long estApproveDate;
    @XmlElement(name = "registration_application_date")
    private long regDate;
    @XmlElement(name = "registration_approval_date")
    private long regApproveDate;
    @XmlElement(name = "approved_party_number")
    private String partyNumber;
    @XmlElement(name = "party_flag")
    private String flag;
    @XmlElement(name = "party_seal")
    private String seal;
    private String region;
    @XmlElement(name = "headquarters")
    private String headAddress;
    private String policy;
    @XmlElement(name = "ST_PCODE")
    private String stPcode;
    @XmlElement(name = "DT_PCODE")
    private String dtPcode;
    @XmlElement(name = "created_at")
    private long creation;
    @XmlElement(name = "updated_at")
    private long modification;
    @XmlElement(name = "chairman")
    private List<String> chairmans = new ArrayList<String>();
    @XmlElement(name = "leadership")
    private List<String> leaders = new ArrayList<String>();
    @XmlElement(name = "contact")
    private List<String> contacts = new ArrayList<String>();

    @XmlTransient
    private Map<String, Integer> legistCount;
    @XmlTransient
    private Map<String, Integer> interestRate;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String geteName() {
	return eName;
    }

    public void seteName(String eName) {
	this.eName = eName;
    }

    public String getAbbreviation() {
	return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
	this.abbreviation = abbreviation;
    }

    public String getMemberCount() {
	return memberCount;
    }

    public void setMemberCount(String memberCount) {
	this.memberCount = memberCount;
    }

    public String getPartyNumber() {
	return partyNumber;
    }

    public void setPartyNumber(String partyNumber) {
	this.partyNumber = partyNumber;
    }

    public String getFlag() {
	return flag;
    }

    public void setFlag(String flag) {
	this.flag = flag;
    }

    public String getSeal() {
	return seal;
    }

    public void setSeal(String seal) {
	this.seal = seal;
    }

    public String getRegion() {
	return region;
    }

    public void setRegion(String region) {
	this.region = region;
    }

    public String getHeadAddress() {
	return headAddress;
    }

    public void setHeadAddress(String headAddress) {
	this.headAddress = headAddress;
    }

    public String getPolicy() {
	return policy;
    }

    public void setPolicy(String policy) {
	this.policy = policy;
    }

    public String getStPcode() {
	return stPcode;
    }

    public void setStPcode(String stPcode) {
	this.stPcode = stPcode;
    }

    public String getDtPcode() {
	return dtPcode;
    }

    public void setDtPcode(String dtPcode) {
	this.dtPcode = dtPcode;
    }

    public long getEstDate() {
	return estDate;
    }

    public void setEstDate(long estDate) {
	this.estDate = estDate;
    }

    public long getEstApproveDate() {
	return estApproveDate;
    }

    public void setEstApproveDate(long estApproveDate) {
	this.estApproveDate = estApproveDate;
    }

    public long getRegDate() {
	return regDate;
    }

    public void setRegDate(long regDate) {
	this.regDate = regDate;
    }

    public long getRegApproveDate() {
	return regApproveDate;
    }

    public void setRegApproveDate(long regApproveDate) {
	this.regApproveDate = regApproveDate;
    }

    public long getCreation() {
	return creation;
    }

    public void setCreation(long creation) {
	this.creation = creation;
    }

    public long getModification() {
	return modification;
    }

    public void setModification(long modification) {
	this.modification = modification;
    }

    public List<String> getChairmans() {
	return chairmans;
    }

    public Map<String, Integer> getLegistCount() {
	return legistCount;
    }

    public Map<String, Integer> getInterestRate() {
	return interestRate;
    }

    public void setInterestRate(Map<String, Integer> interestRate) {
	this.interestRate = interestRate;
    }

    public void setLegistCount(Map<String, Integer> legistCount) {
	this.legistCount = legistCount;
    }

    @SuppressWarnings("unchecked")
    public void setChairmans(Object chairmans) {
	if (chairmans instanceof Map) {
	    Map<String, Object> map = (Map<String, Object>) chairmans;
	    if (null == map.entrySet())
		this.chairmans = Collections.emptyList();
	} else {
	    this.chairmans = (List<String>) chairmans;
	}
    }

    public List<String> getLeaders() {
	return leaders;
    }

    @SuppressWarnings("unchecked")
    public void setLeaders(Object leaders) {
	if (leaders instanceof Map) {
	    Map<String, Object> map = (Map<String, Object>) leaders;
	    if (null == map.entrySet())
		this.leaders = Collections.emptyList();
	} else {
	    this.leaders = (List<String>) leaders;
	}
    }

    public List<String> getContacts() {
	return contacts;
    }

    @SuppressWarnings("unchecked")
    public void setContacts(Object contacts) {
	if (contacts instanceof Map) {
	    Map<String, Object> map = (Map<String, Object>) contacts;
	    if (null == map.entrySet())
		this.contacts = Collections.emptyList();
	} else {
	    this.contacts = (List<String>) contacts;
	}
    }

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
	System.out.println(key);
    }
}
