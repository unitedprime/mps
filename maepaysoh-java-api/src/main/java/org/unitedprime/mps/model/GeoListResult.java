package org.unitedprime.mps.model;

import java.io.Serializable;
import java.util.List;

public class GeoListResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Geo> data;
	private Metadata meta;

	public List<Geo> getData() {
		return data;
	}

	public void setData(List<Geo> data) {
		this.data = data;
	}

	public Metadata getMeta() {
		return meta;
	}

	public void setMeta(Metadata meta) {
		this.meta = meta;
	}

}
