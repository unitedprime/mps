package org.unitedprime.vote;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonAnySetter;

public class Voter implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="dateofbirth")
	private String dobMm;
	private String village;
	@XmlElement(name="father_name")
	private String father;
	@XmlElement(name="nrcno")
	private String nrc;
	private String state;
	@XmlElement(name="voter_name")
	private String voter;
	@XmlElement(name="dateofbirth_num")
	private Date dob;
	@XmlElement(name="mother_name")
	private String mother;
	private String township;
	private String district;
	private List<String> ethnics;

	public String getDobMm() {
		return dobMm;
	}

	public void setDobMm(String dobMm) {
		this.dobMm = dobMm;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getFather() {
		return father;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getVoter() {
		return voter;
	}

	public void setVoter(String voter) {
		this.voter = voter;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(String dobString) {
		try {
			dob = new SimpleDateFormat("dd-MM-yyyy").parse(dobString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}

	public String getTownship() {
		return township;
	}

	public void setTownship(String township) {
		this.township = township;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}
	
	

	public List<String> getEthnics() {
		return ethnics;
	}

	@SuppressWarnings("unchecked")
	public void setEthnics(Object ethnics) {
		try {
			this.ethnics = (List<String>) ethnics;
		} catch (Exception e) {
			
		}
	}

	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		System.out.println(key);
	}
}
