package org.unitedprime.vote;

import java.io.Serializable;
import java.util.Date;

public interface VoterResource extends Serializable{
	
	Voter check(String name, Date dob);
	Voter check(String name, String nrc);

}
