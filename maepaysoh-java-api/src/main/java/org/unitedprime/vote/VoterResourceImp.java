package org.unitedprime.vote;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class VoterResourceImp implements VoterResource {

	private static final long serialVersionUID = 1L;
	private static final String END_POINT = "https://checkvoterlist.uecmyanmar.org/api/";
	private static final String VOTER_NAME = "voter_name";
	private static final String NRC_NUM = "nrcno";
	private static final String DOB = "dateofbirth";

	@Override
	public Voter check(String name, Date dob) {
		return getTarget()
				.queryParam(VOTER_NAME, name)
				.queryParam(DOB, new SimpleDateFormat("yyyy-M-dd").format(dob))
				.request().get(Voter.class);
	}

	@Override
	public Voter check(String name, String nrc) {
		return getTarget().queryParam(VOTER_NAME, name)
				.queryParam(NRC_NUM, nrc).request().get(Voter.class);
	}

	private WebTarget getTarget() {
		return ClientBuilder.newClient().target(END_POINT);
	}

}
