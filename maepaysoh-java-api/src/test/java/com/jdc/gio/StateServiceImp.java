package com.jdc.gio;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.api.GeoResources.Param;
import org.unitedprime.mps.model.Geo;

public class StateServiceImp implements StateService {

	private GeoResources stateResource = GeoResources.getInstance(GeoType.State);
	private Function<Geo, State> mapper = MapperProducer.getStateMapper();

	@Override
	public List<State> getAll() {
		return stateResource.find(TownshipServiceImp.getParam()).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public State findById(String id) {
		return mapper.apply(stateResource.find(id).getData());
	}

	@Override
	public State findByStPcode(String code) {
		Map<Param, Object> params = TownshipServiceImp.getParam();
		params.put(Param.st_pcode, code);

		List<State> list = stateResource.find(params).getData().stream()
				.map(mapper).collect(Collectors.toList());

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	@Override
	public List<State> findByLocation(double lon, double lat) {
		return stateResource.find(lon, lat).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

}
