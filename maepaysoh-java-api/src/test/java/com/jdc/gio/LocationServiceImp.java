package com.jdc.gio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationServiceImp implements LocationService{
	
	private TownshipService tshService = new TownshipServiceImp();
	private DistrictService disService = new DistrictServiceImp();
	private StateService staService = new StateServiceImp();
	
	@Override
	public Map<State, Map<District, List<Township>>> getAllLocations() {
		Map<State, Map<District, List<Township>>> stateMap = new HashMap<>();
		
		List<State> states = staService.getAll();
		
		states.forEach(state -> {
			stateMap.put(state, getDistricts(state));
		});
		
		return stateMap;
	}
	
	private Map<District, List<Township>> getDistricts(State state) {
		Map<District, List<Township>> result = new HashMap<>();
		
		List<District> districts = disService.findByState(state.getStPcode());
		districts.forEach(district -> {
			result.put(district, tshService.findByDistrict(district.getDtPcode()));
		});
		return result;
	}
	

}
