package com.jdc.gio;

import java.util.function.Function;

import org.unitedprime.mps.model.Geo;

public class MapperProducer {
	
	public static Function<Geo, Township> getTownshipMapper() {
		return a -> {
			Township ts = new Township();
			ts.setObjId(Integer.parseInt(a.getProperties().get("OBJECTID")));
			ts.setId(a.getId());
			ts.setName(a.getProperties().get("TS"));
			ts.setMmName(a.getProperties().get("Myanmar3"));
			ts.setStPcode(a.getProperties().get("ST_PCODE"));
			ts.setDtPcode(a.getProperties().get("DT_PCODE"));
			ts.setTsPcode(a.getProperties().get("TS_PCOD"));
			return ts;
		};
	}
	
	public static Function<Geo, District> getDistrictMapper() {
		return a -> {
			District d = new District();
			
			d.setId(a.getId());
			d.setName(a.getProperties().get("DT"));
			d.setMmName(a.getProperties().get("Myanmar3"));
			d.setObjId(Integer.parseInt(a.getProperties().get("OBJECTID")));
			d.setDtPcode(a.getProperties().get("DT_PCODE"));
			d.setStPcode(a.getProperties().get("ST_PCODE"));

			return d;
		};
	}
	
	public static Function<Geo, State> getStateMapper() {
		return a -> {
			State s = new State();
			
			s.setId(a.getId());
			s.setName(a.getProperties().get("ST"));
			// TODO
			s.setMmName(a.getProperties().get("ST_MYA"));
			
			s.setStPcode(a.getProperties().get("ST_PCODE"));
			
			return s;
		};
	}

}
