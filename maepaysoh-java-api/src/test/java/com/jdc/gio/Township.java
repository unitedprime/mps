package com.jdc.gio;

import java.io.Serializable;

public class Township implements Location, Serializable {

	private static final long serialVersionUID = 1L;

	private int objId;
	private String id;
	private String tsPcode;
	private String dtPcode;
	private String stPcode;
	private String name;
	private String mmName;

	public int getObjId() {
		return objId;
	}

	public void setObjId(int objId) {
		this.objId = objId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTsPcode() {
		return tsPcode;
	}

	public void setTsPcode(String tsPcode) {
		this.tsPcode = tsPcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMmName() {
		return mmName;
	}

	public void setMmName(String mmName) {
		this.mmName = mmName;
	}

	public String getDtPcode() {
		return dtPcode;
	}

	public void setDtPcode(String dtPcode) {
		this.dtPcode = dtPcode;
	}

	public String getStPcode() {
		return stPcode;
	}

	public void setStPcode(String stPcode) {
		this.stPcode = stPcode;
	}

	@Override
	public String pcode() {
		return tsPcode;
	}

}
