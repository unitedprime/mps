package com.jdc.gio;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.api.GeoResources.Param;
import org.unitedprime.mps.model.Geo;

public class DistrictServiceImp implements DistrictService {

	private GeoResources distResource = GeoResources.getInstance(GeoType.District);
	private Function<Geo, District> mapper = MapperProducer.getDistrictMapper();

	@Override
	public List<District> getAll() {
		return distResource.find(TownshipServiceImp.getParam()).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public List<District> findByState(String stPcode) {
		Map<Param, Object> params = TownshipServiceImp.getParam();
		params.put(Param.st_pcode, stPcode);
		return distResource.find(params).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public List<District> findByLocation(double lon, double lat) {
		return distResource.find(lon, lat).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public District findById(String id) {
		return mapper.apply(distResource.find(id).getData());
	}

	@Override
	public District findByDtcode(String dtPcode) {

		Map<Param, Object> params = TownshipServiceImp.getParam();
		params.put(Param.dt_pcode, dtPcode);
		
		List<District> list = distResource.find(params).getData().stream()
				.map(mapper).collect(Collectors.toList());
		
		if(list.size() > 0) {
			return list.get(0);
		}
		
		return null;
	}

}
