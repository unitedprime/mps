package com.jdc.gio;

import java.io.Serializable;

public class State implements Location, Serializable {

	private static final long serialVersionUID = 1L;

	private int objId;
	private String id;
	private String stPcode;
	private String name;
	private String mmName;

	public int getObjId() {
		return objId;
	}

	public void setObjId(int objId) {
		this.objId = objId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStPcode() {
		return stPcode;
	}

	public void setStPcode(String stPcode) {
		this.stPcode = stPcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMmName() {
		return mmName;
	}

	public void setMmName(String mmName) {
		this.mmName = mmName;
	}

	@Override
	public String pcode() {
		return stPcode;
	}

}
