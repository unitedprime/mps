package com.jdc.gio;

import java.util.List;

public interface StateService {
	
	List<State> getAll();
	List<State> findByLocation(double lon, double lat);

	State findById(String id);
	State findByStPcode(String code);

}
