package com.jdc.gio;

import java.util.List;

public interface TownshipService {
	
	List<Township> getAll();
	List<Township> findByState(String stPcode);
	List<Township> findByDistrict(String dtPcode);
	List<Township> findByLocation(double lon, double lat);
	
	Township findById(String id);
	Township findByPcode(String tsPcode);
}
