package com.jdc.gio;

import java.util.List;
import java.util.Map;

public interface LocationService {
	Map<State, Map<District, List<Township>>> getAllLocations();
}
