package com.jdc.gio;

import java.util.List;

public interface DistrictService {
	
	List<District> getAll();
	List<District> findByState(String stPcode);
	List<District> findByLocation(double lon, double lat);

	District findById(String id);
	District findByDtcode(String dtPcode);
}
