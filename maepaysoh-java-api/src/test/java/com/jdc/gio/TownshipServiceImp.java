package com.jdc.gio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.api.GeoResources.Param;
import org.unitedprime.mps.model.Geo;

public class TownshipServiceImp implements TownshipService {

	private GeoResources townshipResource = GeoResources.getInstance(GeoType.Township);

	private Function<Geo, Township> mapper = MapperProducer.getTownshipMapper();

	@Override
	public List<Township> getAll() {

		return townshipResource.find(getParam()).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public List<Township> findByState(String stPcode) {

		Map<Param, Object> params = getParam();
		params.put(Param.st_pcode, stPcode);

		return townshipResource.find(params).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public List<Township> findByDistrict(String dtPcode) {
		Map<Param, Object> params = getParam();
		params.put(Param.dt_pcode, dtPcode);

		return townshipResource.find(params).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public List<Township> findByLocation(double lon, double lat) {
		return townshipResource.find(lon, lat).getData().stream().map(mapper)
				.collect(Collectors.toList());
	}

	@Override
	public Township findById(String id) {
		return mapper.apply(townshipResource.find(id).getData());
	}

	@Override
	public Township findByPcode(String tsPcode) {
		Map<Param, Object> params = getParam();
		params.put(Param.ts_pcode, tsPcode);

		List<Township> list = townshipResource.find(params).getData().stream()
				.map(mapper).collect(Collectors.toList());

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public static Map<Param, Object> getParam() {
		Map<Param, Object> params = new HashMap<>();
		params.put(Param.no_geo, true);
		params.put(Param.per_page, 500);
		params.put(Param.page, 1);
		return params;
	}

}
