package com.jdc.mpsapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.api.GeoResources.Param;
import org.unitedprime.mps.model.Geo;
import org.unitedprime.mps.model.GeoListResult;
import org.unitedprime.mps.model.GeoSingleResult;

public class GeoTownshipTest {
	private GeoResources resource;
	
	@Before
	public void init() {
		resource = GeoResources.getInstance(GeoType.Township);
	}
	
	@Ignore
	@Test
	public void test1() {
		GeoListResult result = resource.find();
		Assert.assertNotNull(result);
		Assert.assertEquals(15, result.getData().size());
		
		result.getData().forEach(a -> ShowData.show(a, Geo.class));
	}
	
	@Ignore
	@Test
	public void test2() {
		GeoListResult result = resource.find(96.253221147838, 26.1248220476);
		List<Geo> list = result.getData();
		Assert.assertEquals(2, list.size());
	}
	
	@Ignore
	@Test
	public void test3() {
		GeoSingleResult result = resource.find("55dd5f79358fe46ce2bb8fba");
		Assert.assertNotNull(result.getData());
		Assert.assertEquals("Tanai", result.getData().getProperties().get("Township"));
	}
	
	@Ignore
	@Test
	public void test4() {
		Map<Param, Object> params = new HashMap<>();
		params.put(Param.st_pcode, "MMR001");
		params.put(Param.dt_pcode, "MMR001D001");
		params.put(Param.ts_pcode, "MMR001004");
		
		GeoListResult result = resource.find(params);
		Assert.assertNotNull(result.getData());
		Assert.assertEquals("Tanai", result.getData().get(0).getProperties().get("Township"));
	}
}
