package com.jdc.mpsapi;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.unitedprime.mps.api.CandidateResources;
import org.unitedprime.mps.api.CandidateResources.Gender;
import org.unitedprime.mps.api.CandidateResources.Param;
import org.unitedprime.mps.api.imp.CandidateResourcesImp;
import org.unitedprime.mps.model.Candidate;
import org.unitedprime.mps.model.CandidateSingleResult;
import org.unitedprime.mps.model.Candidates;

public class CandidateTest {

	private static CandidateResources res;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		res = new CandidateResourcesImp();
	}

	@Ignore
	@Test
	public void test1() {
		CandidateSingleResult c = res.findById("55fa683a11b7f310c6884c35");
		ShowData.show(c.getData(), Candidate.class);
	}

	@Ignore
	@Test
	public void test2() {
		Candidates cs = res.findByName("ဦးစိုင်းစိန်အုန်း");
		cs.getData().forEach(a -> ShowData.show(a, Candidate.class));
	}

	@Ignore
	@Test
	public void test3() {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.per_page, 10);
		param.put(Param.gender, Gender.F.toString());
		Candidates cs = res.find(param);
		cs.getData().forEach(a -> ShowData.show(a, Candidate.class));
	}

}
