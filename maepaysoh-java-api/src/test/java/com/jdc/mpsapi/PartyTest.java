package com.jdc.mpsapi;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.unitedprime.mps.api.PartyResources;
import org.unitedprime.mps.api.PartyResources.Param;
import org.unitedprime.mps.api.imp.PartyResourceImp;
import org.unitedprime.mps.model.Parties;
import org.unitedprime.mps.model.Party;
import org.unitedprime.mps.model.PartyMetadata;
import org.unitedprime.mps.model.PartySingleResult;

public class PartyTest {

	private static PartyResources res;

	@BeforeClass
	public static void setUpBeforeClass() {
		res = new PartyResourceImp();
	}

	@Ignore
	@Test
	public void test1() {
		PartySingleResult party = res.findById(55);
		ShowData.show(party.getParty(), Party.class);
	}

	@Ignore
	@Test
	public void test2() {
		Map<Param, Object> param = new HashMap<>();
		param.put(Param.per_page, 3);
		param.put(Param.page, 5);
		Parties party = res.findAll(param);
		party.getData().forEach(a -> ShowData.show(a, Party.class));
		ShowData.show(party.getMeta(), PartyMetadata.class);
	}
}
