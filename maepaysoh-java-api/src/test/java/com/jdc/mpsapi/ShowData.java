package com.jdc.mpsapi;

import java.lang.reflect.Field;

public class ShowData {
	
	public static <T> void show(T t, Class<T> type) {
		System.out.println();
		System.out.println("------------------------------------------");
		for(Field f : type.getDeclaredFields()) {
			f.setAccessible(true);
			try {
				System.out.println(f.getName() + "  =>   " + f.get(t));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				System.out.println("Type Error");
			}
		}
	}

}
