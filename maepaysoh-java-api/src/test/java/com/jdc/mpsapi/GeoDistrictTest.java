package com.jdc.mpsapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.unitedprime.mps.api.GeoResources;
import org.unitedprime.mps.api.GeoResources.GeoType;
import org.unitedprime.mps.api.GeoResources.Param;
import org.unitedprime.mps.model.Geo;
import org.unitedprime.mps.model.GeoListResult;
import org.unitedprime.mps.model.GeoSingleResult;

public class GeoDistrictTest {

    private GeoResources resource;

    @Before
    public void init() {
	resource = GeoResources.getInstance(GeoType.District);
    }

	@Ignore
    @Test
    public void test1() {
	GeoListResult result = resource.find();
	Assert.assertNotNull(result);
    }

    // TODO
    @Ignore
    @Test
    public void test2() {
	GeoListResult result = resource.find(96.253221147838, 26.1248220476);
	List<Geo> list = result.getData();
	Assert.assertEquals(1, list.size());
	Assert.assertEquals("55c064df11b7f310c6882597", list.get(0).getId());
    }

    // TODO
    @Ignore
    @Test
    public void test3() {
	GeoSingleResult result = resource.find("55c064df11b7f310c6882597");
	Assert.assertNotNull(result.getData());
	Assert.assertEquals("Kachin", result.getData().getProperties()
		.get("ST"));
    }

    @Ignore
    @Test
    public void test4() {
	Map<Param, Object> params = new HashMap<>();
	params.put(Param.st_pcode, "MMR001");
	params.put(Param.dt_pcode, "MMR001D002");
	GeoListResult result = resource.find(params);
	Assert.assertNotNull(result.getData());
	Assert.assertEquals("Kachin", result.getData().get(0).getProperties()
		.get("ST"));
    }

}
