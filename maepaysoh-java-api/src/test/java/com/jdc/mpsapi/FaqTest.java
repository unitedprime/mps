package com.jdc.mpsapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.unitedprime.mps.api.FaqResources;
import org.unitedprime.mps.api.FaqResources.Param;
import org.unitedprime.mps.api.imp.FaqResourcesImp;
import org.unitedprime.mps.model.Faq;
import org.unitedprime.mps.model.FaqSingleResult;
import org.unitedprime.mps.model.Faqs;

public class FaqTest {
    private static FaqResources res;

    @BeforeClass
    public static void setUpBeforeClass() {
	res = new FaqResourcesImp();
    }

    @Ignore
    @Test
    public void findByID() {
	FaqSingleResult result = res.findById("55f303545bf730df148b4567");
	Faq a = result.getData();
	ShowData.show(a, Faq.class);
    }

    @Ignore
    @Test
    public void findAll() {
	Map<Param, Object> param = new HashMap<>();
	param.put(Param.per_page, 15);
	Faqs faqs = res.find(param);
	Assert.assertEquals(15, faqs.getData().size());

	faqs.getData().forEach(a -> ShowData.show(a, Faq.class));
    }

    @Test
    public void findByKeyword() {
	Map<Param, Object> param = new HashMap<>();
	param.put(Param.q, "နိုင်ငံတော်");
	Faqs faqs = res.findByKeyword(param);
	List<Faq> faqList = faqs.getData();
	faqList.forEach(a -> ShowData.show(a, Faq.class));
    }
}
