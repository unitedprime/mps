package com.jdc.omiapi;

import org.junit.BeforeClass;
import org.junit.Test;
import org.unitedprime.omi.api.MotionResource;
import org.unitedprime.omi.api.imp.MotionResourceImp;
import org.unitedprime.omi.model.Motion;
import org.unitedprime.omi.model.MotionsByCandidate;

import com.jdc.mpsapi.ShowData;

public class MotionTest {
	
	private static final String MPID = "UPMP-01-0095";
	private static final String ID = "CBM-01-11-003";
	
	private static MotionResource res;
	
	@BeforeClass
	public static void init() {
		res = new MotionResourceImp();
	}
	
	@Test
	public void test1() {
		Motion motion = res.findById(ID);
		ShowData.show(motion, Motion.class);
	}
	
	@Test
	public void test2() {
		MotionsByCandidate ms = res.findByCandidate(MPID);
		ms.getMotions().forEach(a -> ShowData.show(a, Motion.class));
	}

}
