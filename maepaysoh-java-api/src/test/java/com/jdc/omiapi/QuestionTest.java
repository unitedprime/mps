package com.jdc.omiapi;

import org.junit.BeforeClass;
import org.junit.Test;
import org.unitedprime.omi.api.QuestionResource;
import org.unitedprime.omi.api.imp.QuestionResourceImp;
import org.unitedprime.omi.model.Question;
import org.unitedprime.omi.model.QuestionsByCancidate;

import com.jdc.mpsapi.ShowData;

public class QuestionTest {

	static final String MPID = "UPMP-01-0169";
	static final String QID = "UPQ-01-01-001";
	
	static QuestionResource res;
	
	@BeforeClass
	public static void init() {
		res = new QuestionResourceImp();
	}
	
	@Test
	public void test1() {
		Question q = res.findById(QID);
		ShowData.show(q, Question.class);
	}
	
	@Test
	public void test2() {
		QuestionsByCancidate qs = res.findByCandidate(MPID);
		qs.getQuestions().forEach(a -> ShowData.show(a, Question.class));
	}

}
