package com.jdc.voteapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.unitedprime.vote.Voter;
import org.unitedprime.vote.VoterResource;
import org.unitedprime.vote.VoterResourceImp;

import com.jdc.mpsapi.ShowData;

public class VoterTest {

    private static VoterResource res;
    private static final String NAME = "ဇော်မင်းလွင်";
    private static final String NRC = "၁၂/ရကန(နိုင်)၀၀၆၇၆၀";
    private static Date DOB;

    static {
	try {
	    DOB = new SimpleDateFormat("yyyy-MM-dd").parse("1975-01-28");
	} catch (ParseException e) {
	    e.printStackTrace();
	}
    }

    @BeforeClass
    public static void init() {
	res = new VoterResourceImp();
    }

    @Test
    public void test1() {
	Voter voter = res.check(NAME, DOB);
	ShowData.show(voter, Voter.class);
    }

    // TODO
    @Test
    public void test2() {
	Voter voter = res.check(NAME, NRC);
	ShowData.show(voter, Voter.class);
    }

}
